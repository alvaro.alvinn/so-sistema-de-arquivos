# Sistema de arquivos com tagging integrado

### Um estudo de sistema de arquivos e trabalho de conclusão de curso.

Um sistema de aquivos simplificado, baseado em tabela de alocação que teve como objetivo inicialmente servir como estudo do tópico de sistemas de arquivo da disciplina de sistemas operacionais e evoluiu para o sistema base do trabalho de conclusão de curso que busca demostrar a possibilidade da integração de um sistema de tagging a um sistema de arquivos tradicional, baseado em arquivos e diretórios.

### Funcionaldiades

#### Sistema base

- Formatação com tamanho de bloco variável
- Listagem de diretórios
- Criação de arquivos/diretórios
- Escrita em arquivos
- Leitura de arquivos
- Exclusão de arquivos
- Navegação em diretórios
- Pesquisa recursiva por nome

#### Sistema de tagging

- Listagem de tags de arquivo/diretório
- Adição de tags a arquivos/diretórios
- Exclusão de tags de arquivo/diretórios
- Busca por tags
- Busca por nomes e por tags (simultâneamente)


## Build

#### Linux

O comando ``make`` gera um executavel do sistema com nome ``fs``.

#### Windows

Será disponibizado sistema já compilado.

## Uso

O sistema trabalha com discos virtuais

#### Linux

Para criar um disco virtual é utilizado o comando ``truncate``.

Para criar um disco virtual padrão de 10Mb com o nome disk.data é possível utilizar o comando ``make data`` dentro do diretório do programa.

Primeiramente será nescessário formatar o diso, para isso o programa deve receber como argumento as configurações de formatação seguindo o seguinte exemplo:

```console
./fs -f 512 disk.data 
```

``./fs`` é o executavel do sistema, -f indica que será feita a formatação, ``512`` indica o tamanho de cada bloco e por fim ``disk.data`` indica o dispositivo alvo.

Depois disso, para executar o sistema basta passar o nome do disco ja formtado como argumento:

```console
./fs disk.data
```
O programa possui uma interface que simula um interpretador de comandos, digitando `help` é possivel obter uma lista dos comandos disponpíves para o usuário:

```console
q, exit  - Sair
ls       - Listar diretório atual
cd       - Navegar até diretório selecionado
mkdir    - Cria novo diretório
touch    - Cria um arquivo
find     - Encontrar arquivos/diretórios com base em tags e nomes
namefind - Encontrar arquivos/diretórios com base no nome
rm       - Remove arquivo ou diretório
cat      - Mostra o conteúdo do arquivo
echo     - Escreve no arquivo
clear    - Limpa a tela
addtag   - Adiciona tag ao arquivo
rmtag    - Remove tag do arquivo
ltags    - Lista as tags associadas com o arquivo
ctags    - Limpa as tags de um arquivo
tagfind  - Encontrar arquivos/diretórios com base em tag
```


