#ifndef __SISTEMADEARQUIVOS_H__
#define __SISTEMADEARQUIVOS_H__

#ifdef _WIN32
    #include <io.h>
#endif

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>

#include<pthread.h>
#include<semaphore.h>

sem_t semaforo;

#include "../src/dispositivoDeBloco.c"

//#define DEBUG
//#define ULTRA_DEBUG

struct Linha_dir
{
    char nome[26];
    unsigned int endereco;
    bool isDir;
    bool extraFlag;

};

struct Arquivo
{
    char nome[26];                   // nome do arquivo
    unsigned int tam_bytes;          // tamanho atual em bytes 
    unsigned int tam_bloco;          // tamanho atual em bloco 
    unsigned int count_registros;    // quantidade de registros caso seja um diretório
    unsigned int count_tags;         // quantidade de tags associadas com o arquivo
    time_t criado;                   // data de criacao
    time_t modificado;               // data da ultima modificacao
    bool is_dir;                     // é diretorio ou arquivo
    bool is_bin;                     // é um arquivo binario ou de texto
    unsigned int dir;                // diretório do arquivo
    // ------- 64 bytes ------------ //
};

struct FileTags {
    unsigned int proximoBloco;
    unsigned int quantidadeAqui;
};

// Formata o dispositivo
void fs_formatar(char *nomeDev, size_t tamanho_de_bloco);

// Printa toda a tablela
void printaTabela();

// Carrega as configuracaoes de um dispositivo devidamente formatado, retorna -1 caso ocorra erro
int fs_carregar_config();

// Recebe o bloco inicial de um arquivo e retorna uma formatacao no formato struct Linha_dir dele
struct Linha_dir arquivo2Linha_dir(unsigned int arquivo);

// Retorna a table criada, como argumento recebe a quantidade de blocos 
int *criaTabela(unsigned long nEntradas);

// Retorna a qunatidade de blocos que formam o dispositivo
unsigned long getNumEntradas();

// Atualiza tabela em disco com a tabela em memória
void escrever_blocos_tabela();

// Atualiza tabela em memória com a tabela em disco
void ler_blocos_tabela();

// Obtem blococ vazio
int obtem_bloco_vazio();

// Popula a tabela
void popula_tabela();

/**
 * Creates a new directory given the name, iniitla block, and directory block.
 * To create the root directory the deretorio must be 0.
 *
 * @param nome New directory name, must have maximum of 26 characters.
 * @param bloco_incial The initial block to the new directory,
 *                  you usualy want to call \c obtem_bloco_vazio() here.
 * @param diretorio The initial block of the directory of the new directory.
 * @return 1 if success, -1 if not.
 */
int criaDir(const char * nome, unsigned int bloco_inicial, unsigned int diretorio);

// Retorna uma lista com todas as linhas do diretório
struct Linha_dir *lerDiretorio(int n);

// Retorna a quantidade de linhas no diretório
unsigned int getDirCount(int n);

// Retorna o diretorio do arquivo ou diretorio
unsigned int getDir(int n);

// Adiciona uma linha no formato struct Linha_Dir a o diretório especificado pelo bloco inicial
void adicionaLinhaDir(struct Linha_dir linha, int n);

// Printa todas as linhas do diretório espicificado por seu bloco inicial e retorna a lista de todos os itens dentro do diretorio
struct Linha_dir* printDir(int dir);

// Deleta dir
int delDir(unsigned int dir);

// Atualiza diretorio (utiliado por delArquivo)
int AtualizaDir(unsigned int dir);

// Cria um arquivo de texto, recebe nome e bloco inicial
int criaArquivoText(const char *nome, unsigned int bloco_inicial, unsigned int diretorio);

// Escreve um arquivo de texto, recebe um ponteiro com o cotneudo, seu tamanho e o arquivo alvo
int escreveArquivoText(const char* conteudo, unsigned int tam_conteudo, unsigned int arquivo);

// Le o arquivo e retorna um pointeiro com seu conteudo
const char* leArquivoText(unsigned int arquivo);

// Retorna o tamanho em bytes do conteudo do arquivo
unsigned int getTamConteudo(unsigned int arquivo);

// Printa o conteuido de um arquivo de texto
void printArquivoText(unsigned int arquivo);

// Deleta arquivo
int delArquivo(int arquivo);

// Retorna o cabecalho do arquivo ou diretorio especificado
struct Arquivo getCabecalho(int n);

// Retorna o caminho completo
char * getCaminho(unsigned int dir);

// Retorna o Id do arquivo dado o nome e a lista de dir entrys
unsigned int fileFromName(char* nome, struct Linha_dir* lista, int tam);

// Retorna o Id do arquivo que contém um dado dado o nome e a lista de dir entrys
unsigned int fileContainsName(char* nome, struct Linha_dir* lista, int tam);

// Retorna o id do arquivo dado o path
unsigned int pathToFileId(const char* path);

// Adiciona uma tag a um arquivo
int addTagToFile(uint file,  char* tagName);

// Remove uma tag de um arquivo
int removeTagFromFile(uint file,  char* tagName);

// Obtem lista de arquivos que contém determinada tag
int * getFilesFromTag( char* tagName);

// obtem lista de tags de um arquivo
char** getTagsFromFile(unsigned int file, unsigned int* count);

// Cria a estrutura de tag, sempre passar bloco em que a estrutura se encontra
int escreveEstruturaDeTag();

int gravarCabecalho(unsigned int arquivo, struct Arquivo cabecalho);

int createTagInFile(char** listOfTags, unsigned int file);

int gravaTags(char** listOfTags, uint quantity, uint file);

int formataBlocoTag(unsigned int block);

int gravaTagsBloco(char** listOfTags, uint quantity, uint block, uint posicaoAtual);

int clearTagsFromBlock(unsigned int block);

int clearTagsFromFile(unsigned int file);

int lerTags();

int quitFS();
#endif // __SISTEMADEARQUIVOS_H__