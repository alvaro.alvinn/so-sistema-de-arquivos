#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../src/sistemaDeArquivos.c"

struct ThreadInput{
    int argc;
    char ** argv;
};

void cleanInputBuffer();

void clrscr();

int run(int argc, char *argv[]);

#endif