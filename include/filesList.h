#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct linked_list_Temp
{
    unsigned int file;              // 4 bytes
    struct linked_list_Temp *next;  // 8 bytes
} ListNode;

typedef struct
{
    unsigned int count;             // 4 bytes
    unsigned int * counts;          // 8 bytes
    unsigned int ** filesLists;        // 8 bytes
} PersistentLists;

typedef struct
{
    unsigned int count;               // 4 bytes
    ListNode * listPtr;               // 8 bytes
} ListExtended;


/**
 * @brief Given ListNode as the star node, insert the file at the beginning of
 * the list.
 *
 * @param begin pinter to a pointer of the firt listNode
 * @param file  the file identifier to be added
 * 
 */
int addFile(ListNode** begin, unsigned int file);

/**
 * @brief Given ListNode as the star node, remove the specified file from
 * the list.
 *
 * @param begin pinter to a pointer of the firt listNode
 * @param file  the file identifier to be removed
 * 
 */
int removeFile(ListNode** begin, unsigned int file);


int addFile(ListNode** begin, unsigned int file){
    ListNode *novo;
    novo = (ListNode *) malloc(sizeof(ListNode));
    if (novo == NULL){
        printf("Error allocatin memory");
        return 0;
    }
    novo->file = file;
    novo->next = NULL;

    if ((*begin) == NULL){
        (*begin) = novo;
    }
    //se ja ouver um nó inicial, ele virara o segundo e o novo será o inicial
    else{
        novo->next = (*begin);
        (*begin) = novo;

    }
    // interessante aumentar o tamanho, gravar o tamanh na Tag
    return 1;  
}

int removeFile(ListNode** begin, unsigned int file){
    
    if((*begin) == NULL){
        printf("List already empty!\n");
        return 0;
    }

    ListNode* ptr = (*begin);

    if(ptr->file == file){
        (*begin) = ptr->next;
        free(ptr);           
        return 1;
    }

    else{
        ptr = (*begin)->next;
        ListNode *last = (*begin);
        while(ptr != NULL){
            if(ptr->file == file){
                last->next = ptr->next;
                free(ptr);            
                return 1;
            }
            last = ptr;
            ptr = ptr->next;   
        }
        printf("Erro removing, the file is not in the lsit\n");
        return 0;
    }
    return 1;
}

int getListCount(ListNode* begin){
    if(begin == NULL || begin->file == 0){
        return 0;
    }
    else{
        return 1 + getListCount(begin->next);
    }
}

void ListToBuffer(unsigned int** extListPtr, unsigned int * extCount, ListNode* begin){
    int count = getListCount(begin);
    unsigned int * filesList = (unsigned int *)malloc(count * sizeof(unsigned int));
    ListNode * ptr = begin;
    for(int i = 0; i<count; i++ ){
        filesList[i] = (*ptr).file;
        ptr = ptr->next;
    }
    (*extCount) = count;
    (*extListPtr) = filesList;
}

ListNode* bufferToList(int count, unsigned int* fileList){
    if(count == 0){
        return NULL;
    };
    // aloca memória para guardar a lista
    ListNode * ptr = (ListNode *)malloc(count* sizeof(ListNode));
    ListNode * root = ptr;
    // percorre os numeros dos arquivos
    for(int i = 0; i<count; i++){
        ptr[i].file = fileList[i];
        if(i == (count - 1)){
            ptr[i].next = NULL;
        }
        else{
            ptr[i].next = &ptr[i+1];
        }
    }
    return root;
}