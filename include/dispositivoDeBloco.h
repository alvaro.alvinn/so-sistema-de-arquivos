#ifndef BLA_DISPOSITIVO_BLOCO_H
#define BLA_DISPOSITIVO_BLOCO_H
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include "tagTree.h"

struct Dispositivo
{
    char nome[16];                      // nome do dispositivo
    unsigned int fd;                             // id do dispositivo
    unsigned int tamBloco;              // tamanho de cada bloco do dispositivo
    unsigned int countBlocos;           // Quantidade de blocos que formam o dispositivo
    unsigned int countBlocosTabela;     // Quantidade de blocos ocupados pela tabela
    // ---------- 32 bytes ------------ //
    unsigned int dirAtual;              // Diretorio de trabalho atual
    unsigned int raiz;                  // Diretorio raiz
    unsigned int tagStructure;          // Bloco que contém a estrutura de tag

    int *tabela;
    // ---------- 44 bytes ------------ //
};

struct TreeStructure
{   
    unsigned int listsCountsBlock;
    unsigned int listsBlock;
    unsigned int treeBlock;          
    unsigned int treeSize;
    unsigned int listsSize;
    struct BuffersPair buffers;
};

struct BlockTagInfo
{
    unsigned int bytesHere;
    unsigned int nextBlock;
};

// Único, contém as informações do dispositivo assim como a tabela 
static struct Dispositivo dev;

// Ponteiro para o nó inicial da árvore de tags
static TreeNode* tagTree = NULL;

// Estrutura usado para guardar e obter árovore de tags no/do disco
static struct TreeStructure tagStructure = { .listsCountsBlock = 0, .listsBlock = 0, .treeBlock = 0, .treeSize = 0, .listsSize = 0, .buffers = { .tree = NULL, .lists = NULL } };

// Abre um dispositvo, um disco virtual cujo nome do arquivo é passado como argumento
void dev_abrir(const char *nome);
// Fecha o dispositivo que esta aberto
void dev_fechar();
// Lê bloco o bloco n e grava no buffer
void ler_bloco(int n, void *buffer);
// Escreve no bloco n o conteudo do buffer
void escrever_bloco(int n, void *buffer);

#endif