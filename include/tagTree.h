#ifndef TAG_TREE_H
#define TAG_TREE_H
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "filesList.h"

typedef struct {
    unsigned int treeCount;     // 4 bytes
    unsigned int treeSize;      // 4 bytes
    
} TreeInfo;

typedef struct {
    char tagName[24];   // 24 bytes
    ListNode* root;     // 8 bytes
                        // 32 bytes
    
} Tag;

typedef struct tree_node_Temp{

    Tag tag;                       // 32 bytes
    unsigned int height;            // 4 bytes
    struct tree_node_Temp* left;    // 8 bytes
    struct tree_node_Temp* right;   // 8 bytes
    
} TreeNode;

typedef struct BuffersPair{
    
    TreeNode* tree;          
    PersistentLists * lists; 

} BuffersPair;


/**
 * @brief Add a tag to the binary tree, ordering according to the Tag name
 * 
 * @param root Pointer to pointer to the tree root
 * @param tag  The tag to be added
 * @return 1 if succesful 0 if not
 *
 */
TreeNode* addTag(TreeNode** root, Tag* tag);

/**
 * @brief Remove a tag to the binary tree
 * 
 * @param root Pointer to pointer to the tree root
 * @param tag  The tag to be removed
 * @return 1 if succesful 0 if not
 *
 */
TreeNode* removeTag(TreeNode** root, char * tagName);

/**
 * @brief Auxiliary function that return the tram from the tree if the 
 * tag is in it
 * 
 * @param root Pointer to pointer to the tree root
 * @param tagName The tag name to be found
 * @return Tag* char * tagName
 */
Tag* getTag(TreeNode** root, char * tagName);

/**
 * @brief Auxiliary function that return the tram from the tree if the 
 * tree node contaning the Tag with the given tag name
 * 
 * @param root Pointer to pointer to the tree root
 * @param tagName The tag name to be found
 * @return TreeNode* 
 */
TreeNode* getTagNode(TreeNode** root, char * tagName);

/**
 * @brief Add a given file to a given tag in the tree, if the tag 
 * don´t exist create it and insert it in the tree
 * 
 * @param root Pointer to pointer to the tree root
 * @param file File identifier to be inserted
 * @param tagName TagName to be inserted
 * @return 1 if succesful 0 if not 
 */
int addFileToTag(TreeNode** root, unsigned int file, char * tagName);

/**
 * @brief Aemove file from a given tag in the tree, if the tag 
 * don´t exist return 0 and do nothing
 * 
 * @param root Pointer to pointer to the tree root
 * @param file File identifier to be removed
 * @param tagName TagName target
 * @return 1 if succesful 0 if not 
 */
int removeFileFromTag(TreeNode** root, unsigned int file, char * tagName);

/**
 * @brief Print a given treeNode
 * 
 * @param ptr Pointer to a treeNode to be printed
 */
void printTreeNode(TreeNode *ptr);

/**
 * @brief Print a given Tag
 * 
 * @param tag Pointer to a Tag to be printed
 */
void printTag(Tag* tag);

/**
 * @brief used by getCount to count the amount of Tags in the tree
 * 
 * @param ptr Tag beeing counted
 * @param count Ponter to the counter variable
 */
void countTree(TreeNode *ptr, unsigned int* count);

/**
 * @brief Return the number of Tags in the tree
 * 
 * @param ptr Tag beeing counted
 */
unsigned int getCount(TreeNode** root);

// A utility function to get the height of the tree
int height(TreeNode *N)
{
    if (N == NULL)
        return 0;
    return N->height;
}

// A utility function to get maximum of two integers
int max(int a, int b)
{
    return (a > b)? a : b;
}

/* Given a non-empty binary search tree, return the
   node with minimum key value found in that tree.
   Note that the entire tree does not need to be
   searched. */
TreeNode * minValueNode(TreeNode* node)
{
    TreeNode* current = node;
 
    /* loop down to find the leftmost leaf */
    while (current->left != NULL)
        current = current->left;
 
    return current;
}

// A utility function to right rotate subtree rooted with y
TreeNode *rightRotate(TreeNode *y)
{
    TreeNode *x = y->left;
    TreeNode *T2 = x->right;
 
    // Perform rotation
    x->right = y;
    y->left = T2;
 
    // Update heights
    y->height = max(height(y->left),
                    height(y->right)) + 1;
    x->height = max(height(x->left),
                    height(x->right)) + 1;
 
    // Return new root
    return x;
}

// A utility function to left rotate subtree rooted with x
TreeNode *leftRotate(TreeNode *x)
{
    TreeNode *y = x->right;
    TreeNode *T2 = y->left;
 
    // Perform rotation
    y->left = x;
    x->right = T2;
 
    //  Update heights
    x->height = max(height(x->left),  
                    height(x->right)) + 1;
    y->height = max(height(y->left),
                    height(y->right)) + 1;
 
    // Return new root
    return y;
}

TreeNode* rotateLeft(TreeNode* target){
    TreeNode* q;
    TreeNode* temp;
    q = target->left;
    temp = q->right;
    q->right = target;
    target->left = temp;
    target = q;
    // return the top node 
    return target;
}

TreeNode* rotateRight(TreeNode* target){
    TreeNode* q;
    TreeNode* temp;
    q = target->right;
    temp = q->left;
    q->left = target;
    target->right = temp;
    target = q;
    // return the top node
    return target;
}

// Get Balance factor of node N
int getBalance(TreeNode *N)
{
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}

TreeNode* addTag(TreeNode** node, Tag* tag)
{
        /* 1.  Perform the normal BST insertion */
    if (*node == NULL){
        TreeNode *newNode = (TreeNode *) malloc(sizeof(TreeNode));
        newNode->tag = (*tag);
        newNode->left = NULL;
        newNode->right = NULL;
        newNode->height = 1;
        *node = newNode;
        return *node;
    }

    TreeNode* ptr = *node;
    // compare the name to see where to store
    int insertSide = strcmp(tag->tagName,ptr->tag.tagName);
    // lower, insert left
    if(insertSide < 0){
        ptr->left = addTag(&ptr->left, tag);

    }
    else{
        ptr->right = addTag(&ptr->right, tag);
    }

    /* 2. Update height of this ancestor node */
    ptr->height = 1 + max(height(ptr->left),
                        height(ptr->right));
 
    /* 3. Get the balance factor of this ancestor
          node to check whether this node became
          unbalanced */
    int balance = getBalance(ptr);
 
    // If this node becomes unbalanced, then
    // there are 4 cases
 
    // Left Left Case
    
    if (balance > 1 && strcmp(tag->tagName,ptr->left->tag.tagName) < 0){
        ptr = rightRotate(ptr);
        return ptr;
    }
 
    // Right Right Case
    
    if (balance < -1 && strcmp(tag->tagName,ptr->right->tag.tagName) > 0){
        ptr = leftRotate(ptr);
        return ptr;
    }
 
    // Left Right Case
    
    if (balance > 1 && strcmp(tag->tagName,ptr->left->tag.tagName) > 0)
    {
        ptr->left =  leftRotate(ptr->left);
        ptr = rightRotate(ptr);
        return ptr;
    }
 
    // Right Left Case
    
    if (balance < -1 && strcmp(tag->tagName,ptr->right->tag.tagName) < 0)
    {
        ptr->right = rightRotate(ptr->right);
        ptr = leftRotate(ptr);
        return ptr;
    }
 
    /* return the (unchanged) node pointer */
    return ptr;
}

TreeNode* removeTag(TreeNode** root, char * tagName){
    TreeNode* ptr = *root;
    //TreeNode* target;
    //TreeNode* last;

    if (ptr == NULL)
        return ptr;
 
    // If the key to be deleted is smaller than the
    // root's key, then it lies in left subtree
    if (strcmp(tagName, ptr->tag.tagName) < 0)
        ptr->left = removeTag(&ptr->left, tagName);
 
    // If the key to be deleted is greater than the
    // root's key, then it lies in right subtree
    else if(strcmp(tagName, ptr->tag.tagName) > 0)
        ptr->right = removeTag(&ptr->right, tagName);

    else{
        // node with only one child or no child
        if( (ptr->left == NULL) || (ptr->right == NULL) )
        {
            TreeNode *temp = ptr->left ? ptr->left :
                                         ptr->right;
 
            // No child case
            if (temp == NULL)
            {
                temp = ptr;
                ptr = NULL;
            }
            else // One child case
             *ptr = *temp; // Copy the contents of
                            // the non-empty child
            free(temp);
        }
        else
        {
            // node with two children: Get the inorder
            // successor (smallest in the right subtree)
            TreeNode* temp = minValueNode(ptr->right);
 
            // Copy the inorder successor's data to this node
            ptr->tag = temp->tag;
 
            // Delete the inorder successor
            ptr->right = removeTag(&ptr->right, temp->tag.tagName);
        }
            
            // If the tree had only one node then return
            if (ptr == NULL){
                (*root = NULL);
                return ptr;
            }
    }

    // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
    ptr->height = 1 + max(height(ptr->left),
                           height(ptr->right));
 
    // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to
    // check whether this node became unbalanced)
    int balance = getBalance(ptr);
 
    // If this node becomes unbalanced, then there are 4 cases
 
    // Left Left Case
    if (balance > 1 && getBalance(ptr->left) >= 0)
        return rightRotate(ptr);
 
    // Left Right Case
    if (balance > 1 && getBalance(ptr->left) < 0)
    {
        ptr->left =  leftRotate(ptr->left);
        return rightRotate(ptr);
    }
 
    // Right Right Case
    if (balance < -1 && getBalance(ptr->right) <= 0)
        return leftRotate(ptr);
 
    // Right Left Case
    if (balance < -1 && getBalance(ptr->right) > 0)
    {
        ptr->right = rightRotate(ptr->right);
        return leftRotate(ptr);
    }
    return ptr;
}

Tag* getTag(TreeNode** root, char * tagName){
    TreeNode* ptr = *root;

    while(ptr != NULL){
        //if(!((*ptr)->right == NULL && (*ptr)->left == NULL))
            //print((*ptr));

        //verifica se encontrou
        if(strcmp(tagName, ptr->tag.tagName) == 0){
            return &ptr->tag;
        }
        //se não, vai para o proximo nó
        int i = 0;
        for(i = 0; tagName[i] == ptr->tag.tagName[i]; i++){}
        if(tagName[i] < ptr->tag.tagName[i]){
            ptr = ptr->left;
        }
        else{
            ptr = ptr->right;
        }
    }
    return NULL;
}

TreeNode* getTagNode(TreeNode** root, char * tagName){
    TreeNode* ptr = *root;

    while(ptr != NULL){
        //verifica se encontrou
        if(strcmp(tagName, ptr->tag.tagName) == 0){
            return ptr;
        }
        //se não, vai para o proximo nó
        int i = 0;
        for(i = 0; tagName[i] == ptr->tag.tagName[i]; i++){}
        if(tagName[i] < ptr->tag.tagName[i]){
            ptr = ptr->left;
        }
        else{
            ptr = ptr->right;
        }
    }
    return NULL;
}

int addFileToTag(TreeNode** root, unsigned int file, char * tagName){
    // Obtém a tag se existir
    Tag* tag = getTag(root, tagName);
    if(tag != NULL){
        // Caso exista adiciona o arquivo à lista do tag
        addFile(&tag->root, file);
        return 0;
    }
    else{
        // Caso não exista, cria uma nova taga
        Tag* newTag = (Tag *) malloc(sizeof(Tag));
        newTag->root = NULL;
        // Guarda o tamanho da string
        //int count = 0; 
        //for (count = 0; tagName[count] != '\0'; ++count);
        //newTag->nameSize = count + 1;

        // Aloca memória para guardar a string
        // newTag->tagName = (char*)malloc(newTag->nameSize * sizeof(char));
        strcpy(newTag->tagName, tagName);
        // newTag->tagName = tagName;

        // Adiciona o arquivo a lista de tag
        addFile(&newTag->root, file);
        // Adiciona tag a árvore de tag existente
        *root = addTag(root, newTag);
        return 1;
    }
}

int removeFileFromTag(TreeNode** root, unsigned int file, char* tagName){
    // Obtém a tag se existir
    Tag* tag = getTag(root, tagName);
    if(tag != NULL){
        // Caso exista remove o arquivo à lista do tag
        removeFile(&tag->root, file);
        // if list is now empty remove tag
        if(tag->root == NULL)
            removeTag(root, tagName);
        return 1;
    }
    else{
        return 0;
    }
}

void printTreeNode(TreeNode *ptr){
    printf(" |---%s---|\n", ptr->tag.tagName);
    if(ptr->right != NULL && ptr->left != NULL){
        printf("%s        %s\n", ptr->left->tag.tagName, ptr->right->tag.tagName);
    }
    else{
        if(ptr->right == NULL && ptr->left == NULL){
            printf("NULL        NULL\n");
        }
        else{
            if(ptr->left == NULL){
                    printf("NULL     %s\n", ptr->right->tag.tagName);
            }
            if(ptr->right == NULL){
                    printf("%s     NULL\n", ptr->left->tag.tagName);
            }
        }
    }
}

void printTag(Tag* tag){
    printf("%s\n", tag->tagName);
    ListNode* ptr = tag->root;
    while(ptr != NULL){
        printf("%d -> ", ptr->file);
        ptr = ptr->next;
    }
    printf("NULL\n");
}

void printTreeNodeRecursivo(TreeNode *ptr){
    if(strcmp(ptr->tag.tagName,"") != 0){
        printf(" |---%s---|\n", ptr->tag.tagName);
        if(ptr->right != NULL && ptr->left != NULL ){
            if(ptr->right->tag.root == NULL && ptr->left->tag.root == NULL){
                printf("NULL        NULL\n");
            }
            else{
                if(ptr->right->tag.root == NULL){
                    printf("%s     NULL\n", ptr->left->tag.tagName);
                    printTreeNodeRecursivo(ptr->left);
                }
                if(ptr->left->tag.root == NULL){
                    printf("NULL     %s\n", ptr->right->tag.tagName);
                    printTreeNodeRecursivo(ptr->right);
                }
                printf("%s        %s\n", ptr->left->tag.tagName, ptr->right->tag.tagName);
                printTreeNodeRecursivo(ptr->left);
                printTreeNodeRecursivo(ptr->right);
            }

        }
        else{
            if(ptr->right == NULL && ptr->left == NULL){
                printf("NULL        NULL\n");
            }
            else{
                if(ptr->left == NULL){
                        printf("NULL     %s\n", ptr->right->tag.tagName);
                        printTreeNodeRecursivo(ptr->right);
                }
                if(ptr->right == NULL){
                        printf("%s     NULL\n", ptr->left->tag.tagName);
                        printTreeNodeRecursivo(ptr->left);
                }
            }
        }
    }
}

void countTree(TreeNode *ptr, unsigned int* count){
    if(ptr == NULL){
        count = 0;
        return;
    }
    if(ptr->right != NULL && ptr->left != NULL){
        (*count) = (*count) + 1;
        countTree(ptr->left, count);
        countTree(ptr->right, count);
        
    }
    else{
        if(ptr->right == NULL && ptr->left == NULL){
            (*count) = (*count) + 1;
        }
        else{
            if(ptr->left == NULL){
                    countTree(ptr->right, count);
                    (*count) = (*count) + 1;
            }
            if(ptr->right == NULL){
                    countTree(ptr->left, count);
                    (*count) = (*count) + 1;
            }
        }
    }
}

unsigned int getCount(TreeNode** root){
    unsigned int count = 0;
    if((*root) == NULL){
        return 0;
    }
    countTree((*root), &count);
    return count;
}


/// @brief Return the actual number of elements thatare needed to create the buffer
/// @param count the actual number of elemente in the tree, can be obtained by calling *
/// @return Number needed to create the buffer
unsigned int bufferCount(unsigned count);
unsigned int bufferCount(unsigned count){
    if(count == 0){
        return 0;
    }
    uint sum = 1;
    uint i  = 1;
    while(sum < count){
        sum +=+ (uint)pow(2,i);
        i++;
    }
    return sum;
}

// unsued
void TreeToBuffer(TreeNode* node, int number, TreeNode * buffer, int limit){
    if(node != NULL){
        buffer[number] = (*node);
        printf("Buffer[%d] recebeu:\n", number);
        printf("%s\n",buffer[number].tag.tagName);
    }
    else{
        if(number < limit - 1){
            TreeNode empty;
            empty.height = 0;
            empty.left = NULL;
            empty.right = NULL;
            empty.tag.root = NULL;
            buffer[number] = empty;
            printf("Buffer[%d] recebeu: vazio\n", number);
        }
    }
    if (number * 2 < (limit - 1)){
        TreeToBuffer(node->left, (number*2 + 1), buffer, limit);
        TreeToBuffer(node->right, (number*2 + 2), buffer, limit);
    }
}

void TreeToBuffers(TreeNode* node, int number, TreeNode * treeBuffer, PersistentLists* listsStructure, int limit){
    if(node != NULL){
        treeBuffer[number] = (*node);
        treeBuffer[number].left = NULL;
        treeBuffer[number].right = NULL;
        //printf("TreeBuffer[%d] recebeu: ", number);
        //printf("%s\n",treeBuffer[number].tag.tagName);
        ListToBuffer(&listsStructure->filesLists[number], &listsStructure->counts[number], node->tag.root);
        //printf("ListBuffer[%d] recebeu: ", number);
        //printf("Node com arquivo %d\n",listsStructure->filesLists[number][0]);
    }
    else{
        strcpy(treeBuffer[number].tag.tagName, "");
        treeBuffer[number].left = NULL;
        treeBuffer[number].right = NULL;
        //TreeNode test = treeBuffer[number];
        //printf("TreeBuffer[%d] recebeu: vazio\n", number);
        //printf("ListBuffer[%d] recebeu: vazio\n", number);
    }
    if (number * 2 < (limit - 2)){
        if(node != NULL){
            TreeToBuffers(node->left, (number*2 + 1), treeBuffer, listsStructure, limit);
            TreeToBuffers(node->right, (number*2 + 2), treeBuffer,  listsStructure, limit);
        }
        else{
        node = (TreeNode*)malloc(sizeof(TreeNode));
        node->left = NULL;
        node->right = NULL;;
        node->height = 0;
        strcpy(node->tag.tagName, "");
        TreeToBuffers(node->left, (number*2 + 1), treeBuffer, listsStructure, limit);
        TreeToBuffers(node->right, (number*2 + 2), treeBuffer,  listsStructure, limit);
        free(node);
        //printf("TreeBuffer[%d] recebeu: vazio_custom\n", number);
        }
    }
}

// unused
TreeNode* BufferToTree(int number, TreeNode * buffer, int limit);
TreeNode* BufferToTree(int number, TreeNode * buffer, int limit){
    TreeNode* inicio = (TreeNode*)malloc(sizeof(TreeNode));
    (*inicio) = buffer[number];
    if (number * 2 < limit){
        inicio->left = BufferToTree(number*2 + 1, buffer, limit);
        inicio->right = BufferToTree(number*2 + 2, buffer, limit);
    }
    return inicio;
}

/// @brief Auxiliar function used tu recursivly retrive tree information from buffer
/// @param number ferecene the number of the node being retrived
/// @param buffer poiter to the buffer that contain the tree information
/// @param limit buffers size (-2)
/// @return return the poiiter to the tree populated with the information contained in the buffer
TreeNode* BuffersToTree(int number, BuffersPair* buffers, int max);
TreeNode* BuffersToTree(int number, BuffersPair* buffers, int max){
    TreeNode* inicio = (TreeNode*)malloc(sizeof(TreeNode));
    //printf("number %d -  ", number);
    (*inicio) = buffers->tree[number];
    inicio->tag.root = bufferToList(buffers->lists->counts[number], buffers->lists->filesLists[number]);
    if(inicio->tag.root == NULL){
        //printf("tag: NULL\n");
        return NULL;
    }
    //printf("tag: %s\n", inicio->tag.tagName);
    if ((number * 2) < max){
        inicio->left = BuffersToTree(number*2 + 1, buffers, max);
        inicio->right = BuffersToTree(number*2 + 2, buffers, max);
    }
    return inicio;
}

//unused
TreeNode * getBuffer(TreeNode** root);
TreeNode * getBuffer(TreeNode** root){
    unsigned int bCount = bufferCount(getCount(root));
    //printf("Gerando buffer para comportar %d nodes, ou seja, %ld bytes\n", bCount, bCount * sizeof(TreeNode));
    TreeNode * buffer = (TreeNode*)malloc(bCount * sizeof(TreeNode));
    TreeToBuffer((*root), 0, buffer, bCount);
    return buffer;
}


/// @brief Given the pair of bufferrs, that contain the buffers with the tree information and and the structure with the linkedList structures
/// @param pair Empty strucuture to pe filled
/// @param root Tree to where the information will be getten
void getBuffers(BuffersPair* pair, TreeNode** root);
void getBuffers(BuffersPair* pair, TreeNode** root){
    unsigned int bCount = pow(2,(*root)->height) -1;
    //printf("Gerando buffer para comportar %d nodes, ou seja, %ld bytes\n", bCount, bCount * sizeof(TreeNode));
    TreeNode * treeBuffer = (TreeNode*)malloc(bCount * sizeof(TreeNode));
    PersistentLists* listsStructure = (PersistentLists*)malloc(sizeof(listsStructure) + bCount * sizeof(unsigned int));
    unsigned int ** listBuffers = (unsigned int **)malloc(bCount * sizeof(unsigned int *));
    listsStructure->count = bCount;
    listsStructure->counts = (unsigned int*)malloc(bCount * sizeof(unsigned int));
    memset(listsStructure->counts,0, bCount * sizeof(unsigned int));
    listsStructure->filesLists = listBuffers;
    TreeToBuffers((*root), 0, treeBuffer, listsStructure, bCount);
    pair->tree = treeBuffer;
    pair->lists = listsStructure;
    //printf("finalizou! retornando par\n");
}

//unused
TreeNode * getTree(TreeNode* buffer, int count);
TreeNode * getTree(TreeNode* buffer, int count){
    return BufferToTree(0, buffer, count - 2);
}

/// @brief Function you call, return the three with the inforation contained in the pair structure
/// @param buffers structure containing the data 
/// @return pointer to the filled tree
TreeNode * getTreeFromBuffers(BuffersPair* buffers);
TreeNode * getTreeFromBuffers(BuffersPair* buffers){
    return BuffersToTree(0, buffers, buffers->lists->count - 2);
}

/// @brief Prints the tree recursivly
/// @param root Tree
void printTree(TreeNode** root);
void printTree(TreeNode** root){
    if((*root) != NULL){
        printTreeNodeRecursivo((*root));
    }
}


/// @brief used by freeTree
/// @param ptr Treee
void freeTreeNodeRecursivo(TreeNode *ptr);
void freeTreeNodeRecursivo(TreeNode *ptr){
    if(ptr->right != NULL && ptr->left != NULL){
        freeTreeNodeRecursivo(ptr->left);
        freeTreeNodeRecursivo(ptr->right);
        free(ptr);
    }
    else{
        if(ptr->right == NULL && ptr->left == NULL){
            free(ptr);
        }
        else{
            if(ptr->left == NULL){
                    freeTreeNodeRecursivo(ptr->right);
                    free(ptr);
            }
            if(ptr->right == NULL){
                    freeTreeNodeRecursivo(ptr->left);
                    free(ptr);
            }
        }
    }
}

/// @brief Free a tree recusivly
/// @param root Tree to be freed
void freeTree(TreeNode** root);
void freeTree(TreeNode** root){
    freeTreeNodeRecursivo((*root));
}

#endif 

