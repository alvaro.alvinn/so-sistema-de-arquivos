#include "../include/dispositivoDeBloco.h"

void dev_abrir(const char *nome)
{
    if(strlen(nome)>16){
        printf("Só são suportados dispositivos com nomes de no maximo 16 caracteres. Abortando operacao!\n");
        return;
    }
    if (dev.fd != 0)
    {
        close(dev.fd);
    }
    dev.fd = open(nome, O_RDWR); // Read and Write
    lseek(dev.fd, 0, SEEK_SET);   
}

void dev_fechar()
{
    close(dev.fd);
    dev.fd = -1;
}

void ler_bloco(int n, void *buffer)
{
    sem_wait(&semaforo); // Aguarda disponibilidade do semáforo
    //    // Start measuring time
    //struct timespec begin, end; 
    //clock_gettime(CLOCK_REALTIME, &begin);
    lseek(dev.fd, n * dev.tamBloco, SEEK_SET);
    read(dev.fd, buffer, dev.tamBloco);

        // Stop measuring time and calculate the elapsed time
    //clock_gettime(CLOCK_REALTIME, &end);
    //long seconds = end.tv_sec - begin.tv_sec;
    //long nanoseconds = end.tv_nsec - begin.tv_nsec;
    //double elapsed = seconds + nanoseconds*1e-9;
    
    //printf("Result: %.20f\n", sum);
    
    //printf("Time measured: %.10f seconds - IO.\n", elapsed);
    sem_post(&semaforo); // Libera o semáforo
}

void ler_bytes(int n, void *buffer, int bytesCount)
{
    sem_wait(&semaforo); // Aguarda disponibilidade do semáforo
    //struct timespec begin, end; 
    //clock_gettime(CLOCK_REALTIME, &begin);
    lseek(dev.fd, n * dev.tamBloco, SEEK_SET);
    read(dev.fd, buffer, bytesCount);
        //clock_gettime(CLOCK_REALTIME, &end);
    //long seconds = end.tv_sec - begin.tv_sec;
    //long nanoseconds = end.tv_nsec - begin.tv_nsec;
    //double elapsed = seconds + nanoseconds*1e-9;
    
    //printf("Result: %.20f\n", sum);
    
    //printf("Time measured: %.10f seconds - IO.\n", elapsed);
    sem_post(&semaforo); // Libera o semáforo
}

void escrever_bloco(int n, void *buffer)
{
    sem_wait(&semaforo); // Aguarda disponibilidade do semáforo
            // Start measuring time
    //struct timespec begin, end; 
    //clock_gettime(CLOCK_REALTIME, &begin);
    lseek(dev.fd, n * dev.tamBloco, SEEK_SET);
    write(dev.fd, buffer, dev.tamBloco);
        //clock_gettime(CLOCK_REALTIME, &end);
    //long seconds = end.tv_sec - begin.tv_sec;
    //long nanoseconds = end.tv_nsec - begin.tv_nsec;
    //double elapsed = seconds + nanoseconds*1e-9;
    
    //printf("Result: %.20f\n", sum);
    
    //printf("Time measured: %.10f seconds - IO.\n", elapsed);
    sem_post(&semaforo); // Libera o semáforo
}

