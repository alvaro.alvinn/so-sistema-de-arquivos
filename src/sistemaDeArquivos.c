#include "../include/sistemaDeArquivos.h"

void fs_formatar(char *nomeDev, size_t tamanho_de_bloco)
{   // Abre o dispositivo
	dev_abrir(nomeDev);
    // Define o tamanho do bloco
	size_t blk_size = tamanho_de_bloco;
    // Aloca buffer para ser utilizado nas leituras e escritas
	void * buffer = malloc(blk_size);
	
    // Extra informações do dispositivo para estrutura estatica
	strcpy(dev.nome, nomeDev);
	dev.tamBloco = blk_size;
    size_t tamanho_disco = lseek(dev.fd, 0, SEEK_END);
	dev.countBlocos =  tamanho_disco / blk_size; // tamanho do arquivo dividido pelo tamanho de cada bloco
    dev.countBlocosTabela = dev.countBlocos / blk_size;

    // Gera uma nova tabela
    dev.tabela = criaTabela(dev.countBlocos);
    // Popula a tabela 
    popula_tabela();
    
    // Cria o diretorio raiz
    dev.raiz = obtem_bloco_vazio();
    dev.dirAtual = dev.raiz;
    criaDir("/", dev.raiz, 0);

    // Cria a estrutura de tag
    dev.tagStructure = obtem_bloco_vazio();
    escreveEstruturaDeTag();
    dev.tabela[dev.tagStructure] = -1;

    // Define o diretório raiz como o atual
    dev.dirAtual = dev.raiz;

    // Por segurança zera o buffer que sera utilizado 
	memset(buffer, 0, blk_size);

    // Escreve as informações do dispositivo no primeiro bloco do dispositivo
    memcpy(buffer, &dev, sizeof(struct Dispositivo));
	escrever_bloco(0, buffer);

    // Atualiza tabela em disco com a tabela em memória
    escrever_blocos_tabela();

    // Fecha o dispositivo
	dev_fechar();

    // Confere se é possível extrair os dados do dispositivo
    #ifdef DEBUG
        dev.nome[0] = 0;
        dev.tamBloco = 0;
        dev.countBlocos = 0;

        ler_bloco(0, buffer);
        memcpy(&dev, buffer, sizeof(struct Dispositivo));
        printf("Tabela da memória:\n");
        printaTabela();
        printf("populando tabela...\n");
        popula_tabela();
        printf("Tabela da memória:\n");
        printaTabela();
        ler_blocos_tabela();
        printf("nome       : %s\n", dev.nome);
        printf("tamanho    : %dkb\n", tamanho_disco/1024);	
        printf("tamBloco   : %db\n", dev.tamBloco);	
        printf("countBlocos: %d\n", dev.countBlocos);
    #endif
    #ifdef ULTRA_DEBUG
        printf("Tabela da memória:\n");
        printaTabela();
    #endif 
	free(buffer);
}

void printaTabela(){
    printf("TABELA:\n");
    for(size_t i=0; i<dev.countBlocos; i++){
        if(dev.tabela[i] == -1)
            printf("%ld -> %d\n", i, dev.tabela[i]);
    }
}

int fs_carregar_config()
{   
    // Aloca o buffer que sera utilizado
	void * buffer = malloc(sizeof(struct Dispositivo)); 

    // Extrai as informações do dispositivo do primeiro bloco do dispositivo
    dev.tamBloco = sizeof(struct Dispositivo);
	ler_bloco(0, buffer);
    memcpy(&dev, buffer, sizeof(struct Dispositivo));

    // Extrai a tabela do disco
    dev.tabela = criaTabela(dev.countBlocos); // cria tabela na memória
    popula_tabela(); // é preciso popular a tabela na memória antes de ler ela do disco
    ler_blocos_tabela();
    //printaTabela();

    //Extrai as informações da estrutura de Tag
	void * tagBuffer = malloc(sizeof(struct TreeStructure));
    ler_bytes(dev.tagStructure, tagBuffer, sizeof(struct TreeStructure));

    memcpy(&tagStructure, tagBuffer, sizeof(struct TreeStructure));

    tagStructure.buffers.lists = (PersistentLists *)malloc(sizeof(PersistentLists));
    // le árvore de tags
    if(tagStructure.listsCountsBlock != 0 && tagStructure.listsBlock != 0 && tagStructure.treeBlock != 0){
        lerTags();
    }
    

    //printf("Estrutura de tag lida com sucesso:\n");
    //printf("Initial block: %d\n", dev.tagStructure);
    //printf("Size in bytes: %d\n", tagStructure.sizeBytes);
    //printf("Addres to buffers.tree: %s\n", tagStructure.buffers.tree);
    //printf("Addres to buffers.lists: %s\n", tagStructure.buffers.lists);

    if(dev.tamBloco == 0){
        printf("- ERRO - Dispositivo não formatado corretamente,"
        "para formatar execute o programa com a opcao -f\n");
        return -1;
    }
    // Mostra dados extraidos 
    #ifdef DEBUG
        printf("nome       : %s\n", dev.nome);	
        printf("tamBloco   : %d\n", dev.tamBloco);	
        printf("countBlocos: %d\n", dev.countBlocos);
        printf("blksTabela : %d\n", dev.countBlocosTabela);
    #endif
	
    free(tagBuffer);
	free(buffer);
    return 0;
}

int *criaTabela(unsigned long nEntradas)
{
    int *tabela = (int *)malloc(nEntradas * sizeof(int));
    return tabela;
}

unsigned long getNumEntradas()
{

    int tamArquivo = lseek(dev.fd, 0, SEEK_END);

    //printf("tamArquivo = %d\n", tamArquivo);

    unsigned long nEntradas = tamArquivo / dev.tamBloco;
    return nEntradas;
}

struct Arquivo getCabecalho(int n){

    void *buffer = malloc(dev.tamBloco);

    ler_bloco(n, buffer);

    struct Arquivo cabecalho;

    memcpy(&cabecalho, buffer, sizeof(struct Arquivo));

    free(buffer);

    //printf("Cabecalho lido com sucesso!\n");
    return cabecalho;
}

// Escreve a tabela nos blocos
void escrever_blocos_tabela()
{
    int *aux = dev.tabela;
    #ifdef DEBUG
        printf("            Escrevendo tabela no disco...\n");
    #endif

    for(size_t i = 0; i < dev.countBlocosTabela; i++)
    {
        escrever_bloco(i+1, aux);
        #ifdef ULTRA_DEBUG
            //printf("tabela[%d] : %d\n", (dev.tamBloco*i), aux[0]);
            //printf("tabela[%d] : %d\n", (dev.tamBloco*i)+1, aux[1]);
            //printf("tabela[%d] : %d\n", (dev.tamBloco*i)+2, aux[2]);
            //printf("tabela[%d] : %d\n", (dev.tamBloco*i)+3, aux[3]);
            //printf("tabela[%d] : %d\n", (dev.tamBloco*i)+4, aux[4]);
        #endif
        aux += dev.tamBloco;
        
    }
    #ifdef DEBUG
        printf("dir atual: tabela[%d] : %d\n", dev.dirAtual, dev.tabela[dev.dirAtual]);
        printf("            Tabela escrita no disco\n");
    #endif
}

// Verifica se o bloco está vazio
int obtem_bloco_vazio()
{
    // O tamanho da tabela vai ser o numero de entradas desejado * o tamanho de um inteiro
    //long tamanhoTabela = dev.countBlocos * sizeof(int);

    // Percorre a tabela de cima abaixo até encontrar um bloco vazio e retorna sua posicao
    for (unsigned int i = 0; i < dev.countBlocos; i++)
    {
        if (dev.tabela[i] == 0)
        {   
            #ifdef DEBUG
                printf("        O bloco %d está vazio\n", i);
            #endif
            return i;
        }
    }
    printf("- ERRO - Nenhum bloco vazio encontrado\n");
    return -1;
}

void ler_blocos_tabela()
{
    int *aux = dev.tabela;
    #ifdef DEBUG
        printf("Lendo tabela no disco...\n");
    #endif

    for(size_t i = 0; i < dev.countBlocosTabela; i++)
    {
        ler_bloco(i+1, aux);
        #ifdef ULTRA_DEBUG
            //printf("tabela[%d] : %d\n", (dev.tamBloco*i), aux[0]);
            //printf("tabela[%d] : %d\n", (dev.tamBloco*i)+1, aux[1]);
            //printf("tabela[%d] : %d\n", (dev.tamBloco*i)+2, aux[2]);
            //printf("tabela[%d] : %d\n", (dev.tamBloco*i)+3, aux[3]);
            //printf("tabela[%d] : %d\n", (dev.tamBloco*i)+4, aux[4]);
        #endif
        aux += dev.tamBloco;
        
    }
    #ifdef DEBUG
        printf("dir atual: tabela[%d] : %d\n", dev.dirAtual, dev.tabela[dev.dirAtual]);
        printf("Tabela lida do disco\n");
    #endif
}

// Deve ser chamada após definir dev.tabela, dev.countBlocosTabela e dev.countBlocos
void popula_tabela()
{   
    dev.tabela[0] = -1; // primeiro bloco ocupado pela configuracao
    size_t i = 0;
    // Popula os blocos ocupados pela tabela
    for (i; i < dev.countBlocosTabela * (dev.tamBloco / sizeof(int)); i++)
    {
        dev.tabela[i+1] = (i + 1) + 1;
        //printf("%d:%d\n", i, i + 1);
    }
    dev.tabela[i+1] = -1;
    //printf("%d:-1\n", i);
    i++;
    // Inicia o resto dos blocos com 0
    for (i; i < dev.countBlocos-1; i++)
    {
        dev.tabela[i+1] = 0;
        //printf("%d:0\n", i);
    }
}

int criaDir(const char *nome, unsigned int bloco_inicial, unsigned int diretorio)
{
    if (strlen(nome) > 26)
    {
        printf("Nome de diretorio muito grande, deve conter no máximo 26 caracteres, abordatando operacao\n");
        return -1;
    }
    // Obtem o tempo (data) atual
    time_t currentTime = time(NULL);

    // Instancia novo diretório
    struct Arquivo newDir = {
        .tam_bytes = sizeof(struct Arquivo),
        .tam_bloco = 1,
        .count_tags = 0,
        .criado = currentTime,
        .modificado = currentTime,
        .is_dir = true,
        .is_bin = false,
        .dir = diretorio}; 
    strcpy(newDir.nome, nome);

    // Cria buffer de escrita e escreve no bloco inicial do diretorio
    void *buffer = malloc(dev.tamBloco);
    memcpy(buffer, &newDir, sizeof(struct Arquivo));
    escrever_bloco(bloco_inicial, buffer);
    dev.tabela[bloco_inicial] = -1;

    free(buffer);
    // Se não for o diretório raiz que esta sendo criado
    if(diretorio != 0){
        adicionaLinhaDir(arquivo2Linha_dir(bloco_inicial), diretorio);
    }
    // Atualiza tabela em disco com a tabela em memória
    escrever_blocos_tabela();
    return 1;
}

// Grava a estrutura de tag statica, sempre passar bloco da estrutura
int escreveEstruturaDeTag()
{
    uint bloco = dev.tagStructure;
    // Cria buffer de escrita e escreve no bloco inicial
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(bloco, buffer);
    memcpy(buffer, &tagStructure, sizeof(struct TreeStructure));
    //struct BlockTagInfo blockInfo;
    //blockInfo.bytesHere = 0;
    //blockInfo.nextBlock = 0;
    memcpy(buffer + sizeof(struct TreeStructure), &tagStructure, sizeof(struct TreeStructure));
    escrever_bloco(bloco, buffer);

    //printf("ESCREVENDO NO BLOCO %d\n", bloco);
    //printf("Initial block: %d\n", dev.tagStructure);
    //printf("Size in bytes: %d\n", tagStructure.sizeBytes);
    //printf("Addres to buffers.tree: %s\n", tagStructure.buffers.tree);
    //printf("Addres to buffers.lists: %s\n", tagStructure.buffers.lists);

    free(buffer);
    return 1;
}

int formataBlocoTagInfo(unsigned int block){
    // le bloco
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(block, buffer);
    // inicializa estrutura
    struct BlockTagInfo* tagsInfo = (struct BlockTagInfo*)malloc(sizeof(struct BlockTagInfo));
    tagsInfo->nextBlock = 0;
    tagsInfo->bytesHere = 0;
    //grava no bloco
    memcpy(buffer, &tagsInfo, sizeof(struct BlockTagInfo));
    escrever_bloco(block, buffer);
    free(buffer);
    free(tagsInfo);
    return 1;
}

void gravaArvoreBloco(void* treeBuffer, uint treeBytes,  uint blocoAtual){
    //obtem estrutura inical do bloco
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(blocoAtual, buffer);
    void *ptr = buffer;
    void *treePtr = (void*)treeBuffer;
    unsigned int avaliableSpace = dev.tamBloco - sizeof(struct BlockTagInfo);

    // le informaçoes do bloco
    struct BlockTagInfo blockInfo;
    memcpy(&blockInfo, ptr, sizeof(struct BlockTagInfo));
    ptr += sizeof(struct BlockTagInfo);

    //printf("Tamanho disponível no primeiro bloco: %d\n", avaliableSpace);

    // GRAVAÇÃO DE ÁRVORE
    if(treeBytes != 0){
        //printf("Gravando estrutura de árvore!\n");
        //Verifica se ainda é possivel gravar toda a aravore aqui
        if(treeBytes <= avaliableSpace){
            memcpy(ptr, treePtr, treeBytes);
            treePtr+=treeBytes;
            treeBytes = 0;
            avaliableSpace -= treeBytes;
            ptr += treeBytes;
            blockInfo.bytesHere = treeBytes;
        }
        // grava tudo que da da arvore
        else{
            memcpy(ptr, treePtr, avaliableSpace);
            treePtr+=avaliableSpace;
            treeBytes -= avaliableSpace;
            avaliableSpace = 0;
            blockInfo.bytesHere = avaliableSpace;
        }
    }

    if(treeBytes > 0){
        if(blockInfo.nextBlock == 0){
            // aloca novo bloco e grava
            blockInfo.nextBlock = obtem_bloco_vazio();
            //printf("%d\n", blockInfo.nextBlock);
            dev.tabela[blockInfo.nextBlock] = -1;
            escrever_blocos_tabela();
            formataBlocoTagInfo(blockInfo.nextBlock);
            //esvreve as tags no proximo bloco
            gravaArvoreBloco(treePtr, treeBytes, blockInfo.nextBlock);
        }
        else{;
            gravaArvoreBloco(treePtr, treeBytes, blockInfo.nextBlock);
        }
    }

    memcpy(buffer, &blockInfo, sizeof(struct BlockTagInfo));

    escrever_bloco(blocoAtual, buffer);
    return;
    // lembrar de quando o tamanho diminuir, desalocar blocos
}

// toWrite resecebe o total de arquivos nas listas
void gravaListasCountBloco(PersistentLists * lists, uint block, uint toWrite){
    // obtem bloco
    void * buffer = malloc(dev.tamBloco);
    void * ptr = buffer;
    ler_bloco(block, buffer);
    uint avaliableSpace = dev.tamBloco;
    // diminuit o tamanho disponível para gravações e obtem informações do bloco
    struct BlockTagInfo bInfo;
    memcpy(&bInfo, ptr, sizeof(struct BlockTagInfo));
    ptr += sizeof(struct BlockTagInfo);
    avaliableSpace -= sizeof(struct BlockTagInfo);

    //printf("Tamanho disponível no primeiro bloco: %d\n", avaliableSpace);

    // caso seja o primeiro bloco
    // grava quantidade que será gravad (quantidade de listas)
    if(block == tagStructure.listsCountsBlock){
        memcpy(ptr, &lists->count, sizeof(uint));
        ptr += sizeof(uint);
        avaliableSpace -= sizeof(uint);    
    }

    // grava quantidade contida em cada lista
    while(avaliableSpace >= sizeof(uint) && toWrite > 0){
        //printf("gravando lista %d, quantidade: %d\n", tagStructure.buffers.lists->count - toWrite, lists->counts[tagStructure.buffers.lists->count - toWrite]);
        memcpy(ptr, &lists->counts[tagStructure.buffers.lists->count - toWrite], sizeof(uint));
        ptr += sizeof(uint);
        toWrite--;
        avaliableSpace -= sizeof(uint);
    }
    if(toWrite == 0){
            bInfo.nextBlock = 0;
            bInfo.bytesHere = dev.tamBloco - avaliableSpace;
            memcpy(buffer, &bInfo, sizeof(struct BlockTagInfo)); 
            escrever_bloco(block, buffer);
            return;
        }

    if(bInfo.nextBlock == 0){
        bInfo.nextBlock = obtem_bloco_vazio();
        dev.tabela[bInfo.nextBlock] = -1;
        escrever_blocos_tabela();
        gravaListasCountBloco(lists, bInfo.nextBlock, toWrite);
    }
    else{
        gravaListasCountBloco(lists, bInfo.nextBlock, toWrite);
    }

    bInfo.bytesHere = dev.tamBloco - avaliableSpace;

    memcpy(buffer, &bInfo, sizeof(struct BlockTagInfo));

    escrever_bloco(block, buffer);
}

void gravaListasBloco(PersistentLists * lists, uint listsBytes, uint blocoAtual, uint countFiles){
    //obtem estrutura inical do bloco
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(blocoAtual, buffer);
    void *ptr = buffer;
    unsigned int avaliableSpace = dev.tamBloco - sizeof(struct BlockTagInfo);
    uint fileAtual = listsBytes/sizeof(uint) - countFiles;

    // le informaçoes do bloco
    struct BlockTagInfo blockInfo;
    memcpy(&blockInfo, ptr, sizeof(struct BlockTagInfo));
    ptr += sizeof(struct BlockTagInfo);

    //printf("Tamanho disponível no primeiro bloco: %d\n", avaliableSpace);

    // GRAVAÇÃO DE LISTAS

    //printf("Gravando estrutura de lista!\n");
    //printf("Quantidade de listas: %d", lists->count);
    //int curretList = 0;
    int resto = fileAtual;
    //printf("resto inicial: %d\n", resto);
    uint i = 0;
    // avança a até a lista atual
    for(i = 0; i < lists->count; i++){
        //printf("resto = %d - %d\n", resto, lists->counts[i]);
        resto = resto - lists->counts[i];
        if(resto < 0){
            break;
        }
        //printf("já gravou lista %d!\n", i);
    }
    while(avaliableSpace > sizeof(uint) && i < lists->count){
        //printf("resto: %d\n", resto);
        //printf("Gravando lista %d na posição %d: ", i, lists->counts[i] + resto);
        //printf("%d!\n", lists->filesLists[i][lists->counts[i] + resto]);
        // resto -e um valor negativo
        memcpy(ptr, &lists->filesLists[i][lists->counts[i] + resto], sizeof(uint));
        ptr += sizeof(uint);
        resto++;
        avaliableSpace -= sizeof(uint);
        blockInfo.bytesHere += sizeof(uint);
        listsBytes -= sizeof(uint);
        
        if(resto == 0){
            i++;
            if(i >= lists->count){
                break;
            }
            while(lists->counts[i] == 0){
                i++;
                if(i >= lists->count){
                    break;
                }
            }
            if(i < lists->count){
               // printf("resto = %d - %d\n", resto, lists->counts[i]);
                resto = resto - lists->counts[i];
            }

        }
    }
    

    if(listsBytes > 0){
        if(blockInfo.nextBlock == 0){
            // aloca novo bloco e grava
            blockInfo.nextBlock = obtem_bloco_vazio();
            //printf("%d\n", blockInfo.nextBlock);
            dev.tabela[blockInfo.nextBlock] = -1;
            escrever_blocos_tabela();
            formataBlocoTagInfo(blockInfo.nextBlock);
            //esvreve as tags no proximo bloco
            gravaListasBloco(lists, listsBytes, blockInfo.nextBlock, countFiles);
        }
        else{
            //grava no proximo bloco
            gravaListasBloco(lists, listsBytes, blockInfo.nextBlock, countFiles);
        }
    }    
    
    // grava informações do bloco
    memcpy(buffer, &blockInfo, sizeof(struct BlockTagInfo));

    escrever_bloco(blocoAtual, buffer);
    return;
    // lembrar de quando o tamanho diminuir, desalocar blocos
}

void lerListCountBloco(PersistentLists * lists, uint block, uint toRead){
    // obtem bloco
    void * buffer = malloc(dev.tamBloco);
    void * ptr = buffer;
    ler_bloco(block, buffer);
    uint avaliableSpace = dev.tamBloco;
    // obtem informações de bloco
    struct BlockTagInfo bInfo;
    memcpy(&bInfo, ptr, sizeof(struct BlockTagInfo));
    ptr += sizeof(struct BlockTagInfo);
    avaliableSpace -= sizeof(struct BlockTagInfo);

    // caso seja o primeiro bloco
    // le a quantidade de listas e alloca
    if(block == tagStructure.listsCountsBlock){
        memcpy(&lists->count, ptr, sizeof(uint));
        ptr += sizeof(uint);
        toRead = lists->count;
        avaliableSpace -= sizeof(uint);
        tagStructure.buffers.lists->counts = (uint*)malloc(tagStructure.buffers.lists->count * sizeof(uint));
    }



    // le até o fim do bloco, ou até ler tudo
    while(avaliableSpace >= sizeof(uint) && toRead > 0){
        memcpy(&lists->counts[tagStructure.buffers.lists->count - toRead], ptr, sizeof(uint));
        //printf("lendo lista %d, quantidade: %d\n", tagStructure.buffers.lists->count - toRead, lists->counts[tagStructure.buffers.lists->count - toRead]);

        ptr += sizeof(uint);
        toRead--;
        avaliableSpace -= sizeof(uint);
    }
    if(toRead == 0){
            return;
        }

    lerListCountBloco(lists, bInfo.nextBlock, toRead);
}

void lerListsBloco(PersistentLists * lists, uint block, uint countFiles, uint listsBytes){
    //obtem estrutura inical do bloco
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(block, buffer);
    void *ptr = buffer;
    unsigned int avaliableSpace = dev.tamBloco - sizeof(struct BlockTagInfo);
    uint fileAtual = listsBytes/sizeof(uint) - countFiles;

    // le informaçoes do bloco
    struct BlockTagInfo blockInfo;
    memcpy(&blockInfo, ptr, sizeof(struct BlockTagInfo));
    ptr += sizeof(struct BlockTagInfo);

    // GRAVAÇÃO DE LISTAS

    //printf("Lendo estrutura de lista!\n");
    //printf("Quantidade de listas: %d\n", lists->count);
    //int curretList = 0;
    int resto = fileAtual;
    //printf("resto inicial: %d\n", resto);
    uint i = 0;
    // avança a até a lista atual
    for(i = 0; i < lists->count; i++){
        //printf("resto = %d - %d\n", resto, lists->counts[i]);
        resto = resto - lists->counts[i];
        if(resto < 0){
            break;
        }
        //printf("já leu lista %d!\n", i);
    }
    while(avaliableSpace > sizeof(uint) && i < lists->count){
        //printf("resto: %d\n", resto);
        // resto -e um valor negativo
        memcpy(&lists->filesLists[i][lists->counts[i] + resto], ptr, sizeof(uint));
        //printf("Lendo lista %d na posicação %d: %d\n", i, lists->counts[i] + resto, lists->filesLists[i][lists->counts[i] + resto]);
        ptr += sizeof(uint);
        resto++;
        avaliableSpace -= sizeof(uint);
        listsBytes -= sizeof(uint);
        
        if(resto == 0){
            i++;
            if(i >= lists->count){
                break;
            }
            while(lists->counts[i] == 0){
                i++;
                if(i >= lists->count){
                    break;
                }
            }
            if(i < lists->count){
                //printf("resto = %d - %d\n", resto, lists->counts[i]);
                resto = resto - lists->counts[i];
            }

        }
    }
    

    if(listsBytes > 0){
        lerListsBloco(lists, blockInfo.nextBlock, countFiles, listsBytes);
    }    
    
    return;
    // lembrar de quando o tamanho diminuir, desalocar blocos
}

void lerArvoreBloco(void* treeBuffer, uint treeBytes,  uint blocoAtual){
    //obtem estrutura inical do bloco
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(blocoAtual, buffer);
    void *ptr = buffer;
    void *treePtr = (void*)treeBuffer;
    unsigned int avaliableSpace = dev.tamBloco - sizeof(struct BlockTagInfo);

    // le informaçoes do bloco
    struct BlockTagInfo blockInfo;
    memcpy(&blockInfo, ptr, sizeof(struct BlockTagInfo));
    ptr += sizeof(struct BlockTagInfo);

    // LEITURA DE ÁRVORE
    if(treeBytes != 0){
       // printf("Lendo estrutura de árvore!\n");
        //Verifica se é possível ler toda árvore desse bloco
        // se sim le tudo
        if(treeBytes <= avaliableSpace){
            memcpy(treePtr, ptr, treeBytes);
            treePtr+=treeBytes;
            treeBytes = 0;
            avaliableSpace -= treeBytes;
            //ptr += treeBytes;
            //blockInfo.bytesHere = treeBytes;
        }
        // se nao
        // le todo o bloco e continua para ler do próximo
        else{
            memcpy(treePtr, ptr, avaliableSpace);
            treePtr+=avaliableSpace;
            treeBytes -= avaliableSpace;
            avaliableSpace = 0;
            //blockInfo.bytesHere = avaliableSpace;
        }
    }

    if(treeBytes > 0){
        //le do proximo bloco;
        lerArvoreBloco(treePtr, treeBytes, blockInfo.nextBlock);
    }
    return;
    // lembrar de quando o tamanho diminuir, desalocar blocos
}

// Grava a estrutura de tag statica, sempre passar bloco da estrutura
int gravarTags()
{

    // limpar os buffers
    if(tagStructure.buffers.lists != NULL){
        for(uint i = 0; i < tagStructure.buffers.lists->count; i++){
            //printf("LIMPANDO LISTA %d: ", i);
            for(uint j = 0; j < tagStructure.buffers.lists->counts[i]; j++){
                 //printf("%d ", tagStructure.buffers.lists->filesLists[i][j]);
            }
            //printf("\n");
            if(tagStructure.buffers.lists->counts[i] > 0){
                free(tagStructure.buffers.lists->filesLists[i]);
            }
            else{
               //printf("VAZIO< NÃO LIMPOU\n");
            }
        }
        free(tagStructure.buffers.lists->counts);
        free(tagStructure.buffers.tree);
        free(tagStructure.buffers.lists);
    }
    
    getBuffers(&tagStructure.buffers, &tagTree);

   // printf("buffers gerados");
    uint treeSize = (pow(2, tagTree->height) -1)* sizeof(TreeNode); 
    uint listsSize = 0;
    for(uint i = 0; i < tagStructure.buffers.lists->count; i++){
        listsSize += tagStructure.buffers.lists->counts[i] * sizeof(uint);
    }
    //printf("Tamanhos obtidos: \n");

    //printf("treeSize: %d\n", treeSize);
    //printf("listsSize: %d\n", listsSize);

    tagStructure.treeSize = treeSize;
    tagStructure.listsSize = listsSize;
    //printf("gravando erstrutura de tag... \n");
    if(tagStructure.listsCountsBlock == 0){
        tagStructure.listsCountsBlock = obtem_bloco_vazio();
        dev.tabela[tagStructure.listsCountsBlock] = -1;
        escrever_blocos_tabela();
    }
    if(tagStructure.listsBlock == 0){
        tagStructure.listsBlock = obtem_bloco_vazio();
        dev.tabela[tagStructure.listsBlock] = -1;
        escrever_blocos_tabela();
    }
    if(tagStructure.treeBlock == 0){
        tagStructure.treeBlock = obtem_bloco_vazio();
        dev.tabela[tagStructure.treeBlock] = -1;
        escrever_blocos_tabela();
    }

    escreveEstruturaDeTag();

    //ok
    //printf("gravando lista de count...\n");
    //printf("Número de listas: %d\n", tagStructure.buffers.lists->count);

    //printf("Tamanho do count das listas: %ld", (tagStructure.buffers.lists->count) * sizeof(uint));

    gravaListasCountBloco(tagStructure.buffers.lists, tagStructure.listsCountsBlock, tagStructure.buffers.lists->count);

    //printf("Tamanho das listas: %d", listsSize);

    //printf("gravando lista...\n");
    gravaListasBloco(tagStructure.buffers.lists, listsSize, tagStructure.listsBlock, listsSize/sizeof(uint));

    //printf("Tamanho das arvores: %d", treeSize);

    //printf("gravando arvore...\n");
    gravaArvoreBloco((void*)tagStructure.buffers.tree, treeSize, tagStructure.treeBlock);
    //gravarTagsBloco(&tagStructure.buffers, treeSize, listsSize, dev.tagStructure, listsSize/sizeof(uint));
    return 1;
}

int lerTags()
{   
    
    //printf("ESTRUTURA ANTERIOR:\n");
    //printf("Número de tags: %d\n", tagStructure.buffers.lists->count);    
    //printf("listas:\n");
    //for(uint i = 0; i<tagStructure.buffers.lists->count; i++){
    //    printf("%d: ", i);
    //    for(uint j = 0; j<tagStructure.buffers.lists->counts[i]; j++){
    //        printf("%d ", tagStructure.buffers.lists->filesLists[i][j]);
    //    }
    //    printf("\n");
    //}
    //printf("ARVORE:\n");
    //if(tagTree != NULL){
    //    printTree(&tagTree);
    //}

    //printf("Lendo quantidade de tags/listas...\n");
    lerListCountBloco(tagStructure.buffers.lists, tagStructure.listsCountsBlock, 0);

    tagStructure.buffers.lists->filesLists = (uint **)malloc(tagStructure.buffers.lists->count * sizeof(uint*));
    for(uint i = 0; i<tagStructure.buffers.lists->count; i++){
        tagStructure.buffers.lists->filesLists[i] = (uint*)malloc(tagStructure.buffers.lists->counts[i] * sizeof(uint));
    }

    lerListsBloco(tagStructure.buffers.lists, tagStructure.listsBlock, tagStructure.listsSize/sizeof(uint), tagStructure.listsSize);

    tagStructure.buffers.tree = (TreeNode*)malloc(tagStructure.treeSize);

    lerArvoreBloco((void*)tagStructure.buffers.tree, tagStructure.treeSize,  tagStructure.treeBlock);


    //lerTagsBloco(&tagStructure.buffers, tagStructure.treeSize, tagStructure.listsSize, dev.tagStructure, tagStructure.listsSize/sizeof(uint));
    if(tagStructure.treeSize > 0 && tagStructure.listsSize > 0){
        tagTree = getTreeFromBuffers(&tagStructure.buffers);
        /*
        printf("ESTRUTURA LIDA:\n");
        printf("Número de tags: %d\n", tagStructure.buffers.lists->count);    
        printf("listas:\n");
        for(uint i = 0; i<tagStructure.buffers.lists->count; i++){
            printf("%d: ", i);
            for(uint j = 0; j<tagStructure.buffers.lists->counts[i]; j++){
                printf("%d ", tagStructure.buffers.lists->filesLists[i][j]);
            }
            printf("\n");
        }
        printf("ARVORE:\n");
        printTree(&tagTree);
        */
    }
    return 1;
}

struct Linha_dir *lerDiretorio(int dir)
{

    void *buffer = malloc(dev.tamBloco);

    // Lê o cabecalho do diretorio
    ler_bloco(dir, buffer);

    struct Arquivo cabecalho;

    memcpy(&cabecalho, buffer, sizeof(struct Arquivo));

    //printf("\e[1m" "\t%.26s\n" "\e[m", cabecalho.nome);

    int tam_linha_dir = sizeof(struct Linha_dir);

    // Bloco atual = bloco de dados, descartando cabecalho
    int bloco_atual = dev.tabela[dir];

    // Cria lista que gaurda todos os registos do diretório
    struct Linha_dir *lista = (struct Linha_dir *)malloc(cabecalho.count_registros * tam_linha_dir); // <-- 1 Bytes
    int bloco = 0;
    struct Linha_dir *aux = lista;
    //printf("TAMANHO: %d\n", cabecalho.count_registros);
    //printf("PASTA: %s\n", cabecalho.nome);
    while (bloco_atual != -1)
    {
        if(bloco_atual == 0){
            printf("ERRO - ACESSANDO BLOCO LIVRE\n");
            break;
        }
        //printf("BLOCO ATUAL: %d\n", bloco_atual);
        // Lê os dados do diretório
        ler_bloco(bloco_atual, buffer);
        struct Linha_dir * ptr = (struct Linha_dir *)buffer;
        bloco_atual = dev.tabela[bloco_atual];
        if (bloco_atual != -1)
        {
            for (unsigned int i = 0; i < (dev.tamBloco / tam_linha_dir); i++)
            {
                memcpy(&aux[bloco*(dev.tamBloco / tam_linha_dir) + i], &ptr[i], tam_linha_dir); // <-- 8 Bytes
                //printf("POSICAO: %d\n", bloco*(dev.tamBloco / tam_linha_dir) + i);
            } 
        }
        // Caso seja o ultimo bloco só são lidas as "linhas" ocupadas, e não todo o bloco
        else
        {
            for (unsigned int i = 0; i < (cabecalho.count_registros % (dev.tamBloco / tam_linha_dir)); i++)
            {
                memcpy(&aux[bloco*(dev.tamBloco / tam_linha_dir) + i], &ptr[i], tam_linha_dir);
                //printf("POSICAO F: %d\n", bloco*(dev.tamBloco / tam_linha_dir) + i);
            }
        }
        bloco++;
    }
    free(buffer);
    return lista;
}

unsigned int getDirCount(int n)
{

    void *buffer = malloc(dev.tamBloco);

    ler_bloco(n, buffer);

    struct Arquivo cabecalho;

    memcpy(&cabecalho, buffer, sizeof(struct Arquivo));

    free(buffer);

    return cabecalho.count_registros;
}

unsigned int getDir(int n)
{

    void *buffer = malloc(dev.tamBloco);

    ler_bloco(n, buffer);

    struct Arquivo cabecalho;

    memcpy(&cabecalho, buffer, sizeof(struct Arquivo));

    free(buffer);

    return cabecalho.dir;
}

void adicionaLinhaDir(struct Linha_dir linha, int n)
{   
    int tam_linha_dir = sizeof(struct Linha_dir);

    void *buffer = malloc(dev.tamBloco);

    struct Arquivo cabecalho = getCabecalho(n);


    // Obtem quantos registros tem no bloco atual
    unsigned int ultimo_registro_bloco = cabecalho.count_registros % (dev.tamBloco / sizeof(struct Linha_dir));

    int bloco_atual = dev.tabela[n];

    int ultimo_bloco = n;
    // Vai até o ultimo bloco alocado pelo diretorio
    while (bloco_atual != -1)
    {
        ultimo_bloco = bloco_atual;
        bloco_atual = dev.tabela[ultimo_bloco];
    }

    // Se for necessario adicionar novo bloco
    if (ultimo_registro_bloco == 0)
    {
        // obtem novo bloco vazio
        bloco_atual = obtem_bloco_vazio();
        // ultimo bloco aponta para o novo bloco criado
        dev.tabela[ultimo_bloco] = bloco_atual;
        // bloco criado se torna o ultimo
        dev.tabela[bloco_atual] = -1;
        // setta o novo bloco como ultimo
        ultimo_bloco = bloco_atual;
        cabecalho.tam_bloco+= 1;
    }


    // Atualiza o cabecalho
    cabecalho.count_registros++;
    cabecalho.tam_bytes+= tam_linha_dir;
    cabecalho.modificado = time(NULL);
    // Grava alteracoes
    memcpy(buffer, &cabecalho, sizeof(struct Arquivo));
    escrever_bloco(n, buffer);
    

    // Adiciona registro
    ler_bloco(ultimo_bloco, buffer);

    memcpy(buffer + (tam_linha_dir * ultimo_registro_bloco), &linha, tam_linha_dir);

    escrever_bloco(ultimo_bloco, buffer);

    free(buffer);
}

struct Linha_dir* printDirOld(int dir)
{
    if(dev.tabela[dir] != 0){
        // Pega a quantidade de itens do diretório
        unsigned int tam = getDirCount(dir);
        // Se não for  0
        if (tam > 0)
        {
            // Cria um ponteiro array para todas as linhas
            struct Linha_dir *lista = (struct Linha_dir *)malloc(tam * sizeof(struct Linha_dir));
            // Recebe as linhas do diretório
            lista = lerDiretorio(dir);
            struct Linha_dir* retorno = lista;
            for (unsigned int i = 0; i < tam - 1; i++)
            {   
                // Caso seja um diretório é printado como negrito
                if(getCabecalho(lista->endereco).is_dir){
                    #ifdef _WIN32
                        _setmode(_fileno(stdout), _O_U16TEXT);
                        wprintf(L"\t\x251c ");
                        _setmode(_fileno(stdout), _O_TEXT);
                        printf("\e[1m" "%-26s | %d\n" "\e[m", lista->nome, lista->endereco);
                    #else
                        printf("\e[1m" "\t├ %-26s | %d\n" "\e[m", lista->nome, lista->endereco);
                    #endif
                }
                else{
                    #ifdef _WIN32
                        _setmode(_fileno(stdout), _O_U16TEXT);
                        wprintf(L"\t\x251c ");
                        _setmode(_fileno(stdout), _O_TEXT);
                        printf("%-26s | %d\n", lista->nome, lista->endereco);
                    #else
                        printf("\t├ %-26s | %d\n", lista->nome, lista->endereco);
                    #endif
                    
                }
                lista++;
            }
            // Caso seja um diretório é printado como negrito
            if(getCabecalho(lista->endereco).is_dir){
                #ifdef _WIN32
                    _setmode(_fileno(stdout), _O_U16TEXT);
                    wprintf(L"\t\x2514 ");
                    _setmode(_fileno(stdout), _O_TEXT);
                    printf("\e[1m" "%-26s | %d\n" "\e[m", lista->nome, lista->endereco);
                #else
                    printf("\e[1m" "\t└ %-26s | %d\n" "\e[m", lista->nome, lista->endereco);
                #endif
            }
            else{
                #ifdef _WIN32
                    _setmode(_fileno(stdout), _O_U16TEXT);
                    wprintf(L"\t\x2514 ");
                    _setmode(_fileno(stdout), _O_TEXT);
                    printf("%-26s | %d\n", lista->nome, lista->endereco);
                #else
                    printf("\t└ %-26s | %d\n", lista->nome, lista->endereco);
                #endif
            }
            return retorno;
        }
        else
        {
            printf("Diretório vazio!\n");
            return NULL;
        }
    }
    else{
        printf("Diretorio inexistente!\n");
        return NULL;
    }

    
}

struct Linha_dir* printDir(int dir)
{
    if(dev.tabela[dir] != 0){
        // Pega a quantidade de itens do diretório
        unsigned int tam = getDirCount(dir);
        // Se não for  0
        if (tam > 0)
        {
            // Cria um ponteiro array para todas as linhas
            struct Linha_dir *lista = (struct Linha_dir *)malloc(tam * sizeof(struct Linha_dir));
            // Recebe as linhas do diretório
            lista = lerDiretorio(dir);
            struct Linha_dir* retorno = lista;
            for (unsigned int i = 0; i < tam - 1; i++)
            {   
                // Caso seja um diretório é printado como negrito
                if(getCabecalho(lista->endereco).is_dir){
                    #ifdef _WIN32
                        _setmode(_fileno(stdout), _O_U16TEXT);
                        wprintf(L"\t\x251c ");
                        _setmode(_fileno(stdout), _O_TEXT);
                        printf("\e[1m" "%-26s\n" "\e[m", lista->nome);
                    #else
                        printf("\e[1m" "%-26s\n" "\e[m", lista->nome);
                    #endif
                }
                else{
                    #ifdef _WIN32
                        _setmode(_fileno(stdout), _O_U16TEXT);
                        wprintf(L"\t\x251c ");
                        _setmode(_fileno(stdout), _O_TEXT);
                        printf("%-26s\n", lista->nome);
                    #else
                        printf("%-26s\n", lista->nome);
                    #endif
                    
                }
                lista++;
            }
            // Caso seja um diretório é printado como negrito
            if(getCabecalho(lista->endereco).is_dir){
                #ifdef _WIN32
                    _setmode(_fileno(stdout), _O_U16TEXT);
                    wprintf(L"\t\x2514 ");
                    _setmode(_fileno(stdout), _O_TEXT);
                    printf("\e[1m" "%-26s\n" "\e[m", lista->nome);
                #else
                    printf("\e[1m" "%-26s\n" "\e[m", lista->nome);
                #endif
            }
            else{
                #ifdef _WIN32
                    _setmode(_fileno(stdout), _O_U16TEXT);
                    wprintf(L"\t\x2514 ");
                    _setmode(_fileno(stdout), _O_TEXT);
                    printf("%-26s\n", lista->nome);
                #else
                    printf("%-26s\n", lista->nome);
                #endif
            }
            return retorno;
        }
        else
        {
            printf("Diretório vazio!\n");
            return NULL;
        }
    }
    else{
        printf("Diretorio inexistente!\n");
        return NULL;
    }

    
}

int recursiveDel(int n){
    if (n == -1){
        return 0;
    }
    else{
        dev.tabela[n] = recursiveDel(dev.tabela[n]);
    }
    return 1;
}

int delDir(unsigned int dir){
    if(dev.tabela[dir] !=0 && dir > dev.raiz){
        // Pega a quantidade de itens do diretório
        unsigned int tam = getDirCount(dir);
        unsigned int dir_do_dir = getDir(dir);
        // Se não for  0
        if (tam > 0)
        {   
            //printf("    Diretório possui arquivos, deletando arquivos...\n");
            // Cria um ponteiro array para todas as linhas
            struct Linha_dir *lista = (struct Linha_dir *)malloc(tam * sizeof(struct Linha_dir));
            // Recebe as linhas do diretório
            lista = lerDiretorio(dir);
            for (unsigned int i = 0; i < tam - 1; i++)
            {   
                //printf("    DELETANDO ARQUIVO %d\n", lista->endereco);
                recursiveDel(lista->endereco);
                lista++;
            }
            //printf("    DELETANDO ARQUIVO %d\n", lista->endereco);
            recursiveDel(lista->endereco);

            //printf("    Diretório vazio, deletando!\n");
        }
        else
        {
            //printf("    Diretório ja vazio, deletando!\n");
        }
        recursiveDel(dir);
        dev.tabela[dir] = -2;
        //printf("Diretorio estava na posicao %d, que agora aponta para %d\n", dir, dev.tabela[dir]);
        //printf("Atualizando diretorio %d\n", dir_do_dir);
        AtualizaDir(dir_do_dir);
        escrever_blocos_tabela();
        return 1;
    }
    else{
        if(dir <= dev.raiz){
            printf("voce não pode delter o diretorio raiz\n");
            return -1;
        }
        else{
            //printf("Diretorio inexistente!\n");
            return -1;
        }
    }
    
}



int delArquivo(int arquivo){
    if(dev.tabela[arquivo] != 0){
        struct Arquivo cabecalho = getCabecalho(arquivo);
        unsigned int dir = cabecalho.dir;
        dev.tabela[arquivo] = -2;
        //printf("tabela atualizada, posicao %d agora aponta pra %d\n", arquivo, dev.tabela[arquivo]);
        AtualizaDir(dir);
        escrever_blocos_tabela();
    }
    else{
        //printf("arquivo inexistente, abortando operacao de exclusao\n");
    }
    return 1;
}

int AtualizaDir(unsigned int dir){
    // Pega a quantidade de itens do diretório
    //printf("    Atualizando diretório modificado\n");
    unsigned int tam = getDirCount(dir);
    struct Linha_dir *lista = (struct Linha_dir *)malloc(tam * sizeof(struct Linha_dir));
    // Se não for  0
    if (tam > 0)
    {        
        // Recebe as linhas do diretório
        //printf("    Obtendo arquivos existentes no diretorio\n");
        lista = lerDiretorio(dir);

    }
    else{
        //printf("Diretorio Vazio\n");
        return -1;
    }
    //printf("    Redefinindo diretorio...\n");
    
    struct Arquivo cabecalho = getCabecalho(dir);
    cabecalho.count_registros = 0;
    cabecalho.tam_bloco = 1;
    cabecalho.tam_bytes = sizeof(struct Arquivo);
    cabecalho.modificado = time(NULL);

    recursiveDel(dir);
    void *buffer = malloc(dev.tamBloco);
    memcpy(buffer, &cabecalho, sizeof(struct Arquivo));
    escrever_bloco(dir, buffer);
    free(buffer);
    dev.tabela[dir] = -1;
    //printf("    Readicionando entradas no diretorio...\n");
    
    for(unsigned int i = 0; i<tam; i++){
        //printf("Verificando endereco %d.... = %d\n", lista[i].endereco, dev.tabela[lista[i].endereco]);
        if(dev.tabela[lista[i].endereco] != -2){
            adicionaLinhaDir(lista[i],dir);
            //printf("Adicionado...\n");
        }
        else{
           // printf("    Alteracao detectada\n");
           // printf("    arquivo excuido: %s\n", lista[i].nome);
            dev.tabela[lista[i].endereco] = 0;
        }
    }
    free(lista);
    return 1;
}

int criaArquivoText(const char *nome, unsigned int bloco_inicial, unsigned int diretorio)
{   
    if (strlen(nome) > 26)
    {
        //printf("Nome de arquivo muito grande, deve conter no máximo 26 caracteres, abordatando operacao\n");
        return -1;
    }
    // Obtem o tempo (data) atual
    time_t currentTime = time(NULL);
    // Instancia novo diretório
    struct Arquivo newArquivo = {
        .tam_bytes = sizeof(struct Arquivo),
        .tam_bloco = 1,
        .count_tags = 0,
        .criado = currentTime,
        .modificado = currentTime,
        .is_dir = false,
        .is_bin = false,
        .dir = diretorio};
    strcpy(newArquivo.nome, nome);
    // Cria buffer de escrita e escreve no bloco inicial
    void *buffer = malloc(dev.tamBloco);
    memcpy(buffer, &newArquivo, sizeof(struct Arquivo));
    escrever_bloco(bloco_inicial, buffer);
    //inicializa informações de tag
    struct BlockTagInfo tagsInfo;
    tagsInfo.nextBlock = 0;
    tagsInfo.bytesHere = 0;
    //grava no bloco
    memcpy(buffer + sizeof(struct Arquivo), &tagsInfo, sizeof(struct BlockTagInfo));
    dev.tabela[bloco_inicial] = -1;
    free(buffer);
    adicionaLinhaDir(arquivo2Linha_dir(bloco_inicial), diretorio);
    escrever_blocos_tabela();
    return 1;
}

int escreveArquivoText(const char* conteudo, unsigned int tam_conteudo, unsigned int arquivo){
    unsigned int blocos = tam_conteudo/dev.tamBloco;
    if(tam_conteudo%dev.tamBloco>0){
        blocos++;
    }

    // Atualiza cabecalho
    void *buffer = malloc(dev.tamBloco);

    ler_bloco(arquivo, buffer);

    struct Arquivo cabecalho;

    memcpy(&cabecalho, buffer, sizeof(struct Arquivo));

    cabecalho.tam_bloco += blocos;
    cabecalho.tam_bytes += tam_conteudo;
    
    // Grava alteracoes
    memcpy(buffer, &cabecalho, sizeof(struct Arquivo));
    escrever_bloco(arquivo, buffer);

    // Faz a escrita
    int bloco_atual = arquivo;
    int bloco_anterior = arquivo;

    for(unsigned int i=0; i<blocos; i++){
        //printf("    alocando um bloco para a escrita...\n");
        // Atualiza tabela
        bloco_atual = obtem_bloco_vazio();
        // Escreve o conteudo no bloco atual
        dev.tabela[bloco_anterior] = bloco_atual;
        dev.tabela[bloco_atual] = -1;
        memcpy(buffer, conteudo, dev.tamBloco);
        escrever_bloco(bloco_atual, buffer);
        // Prepara para a proxima escrita
        conteudo += dev.tamBloco;
        bloco_anterior = bloco_atual;
    }
    free(buffer);
    escrever_blocos_tabela();
    return 1;
}

const char* leArquivoText(unsigned int arquivo){

    
    // Atualiza cabecalho
    void *buffer = malloc(dev.tamBloco);

    ler_bloco(arquivo, buffer);

    struct Arquivo cabecalho;

    memcpy(&cabecalho, buffer, sizeof(struct Arquivo));

    //unsigned int blocos = cabecalho.tam_bloco -1;
    char * conteudo = (char*)malloc(cabecalho.tam_bytes - sizeof(struct Arquivo));
    #ifdef DEBUG
        printf("    Lendo Arquivo: %s...\n", cabecalho.nome);
    #endif
    // Vai apra o primeiro bloco de dados
    int bloco_atual = dev.tabela[arquivo];
    int bloco_anterior = arquivo;

    if (bloco_atual == -1){
        printf("Arquivo vazio!");
        return NULL;
    }

    char *aux = conteudo;

    // Lê enquanto não chega no ultimo bloco
    while(bloco_atual != -1){
        // caso for o ultimo bloco de leitura
        if(dev.tabela[bloco_atual] == -1){
            #ifdef DEBUG
                printf("    lendo ultimo bloco de texto...\n");
            #endif
            // Lê o conteudo do bloco
            ler_bloco(bloco_atual, buffer);
            memcpy(aux, buffer, (cabecalho.tam_bytes - sizeof(struct Arquivo))%dev.tamBloco);
            // Setta o proximo bloco
            bloco_anterior = bloco_atual;
            bloco_atual = dev.tabela[bloco_anterior];
        }
        else{
            #ifdef DEBUG
                printf("    lendo bloco de texto...\n");
            #endif
            // Lê o conteudo do bloco
            ler_bloco(bloco_atual, buffer);
            memcpy(aux, buffer, dev.tamBloco);
            // Setta o proximo bloco
            aux += dev.tamBloco;
            bloco_anterior = bloco_atual;
            bloco_atual = dev.tabela[bloco_anterior];
        }
    }
    free(buffer);
    return conteudo;
}

unsigned int getTamConteudo(unsigned int arquivo){

    void *buffer = malloc(dev.tamBloco);

    ler_bloco(arquivo, buffer);

    struct Arquivo cabecalho;

    memcpy(&cabecalho, buffer, sizeof(struct Arquivo));

    free(buffer);
    return cabecalho.tam_bytes - sizeof(struct Arquivo);
}

void printArquivoText(unsigned int arquivo){
    unsigned int tam= getTamConteudo(arquivo);
    #ifdef DEBUG
        printf("tam: %d\n", tam);
    #endif
    if(tam == 0){
        printf("\nArquivo vazio!\n");
        return;
    }
    char conteudo[tam];
    strcpy(conteudo, leArquivoText(arquivo));
    printf("%s\n", conteudo);
}

struct Linha_dir arquivo2Linha_dir(unsigned int arquivo){
    void *buffer = malloc(dev.tamBloco);

    ler_bloco(arquivo, buffer);

    struct Arquivo cabecalho;

    memcpy(&cabecalho, buffer, sizeof(struct Arquivo));   

    struct Linha_dir linha_dir = {.endereco = arquivo};

    strcpy(linha_dir.nome, cabecalho.nome);

    free(buffer);

    return linha_dir;
}

char * getCaminho(unsigned int dir){
    if(dir == 0){
        printf("ERRO - LENDO DIRETÓRIO 0!\n");
        return 0;
    }
    if(dir == dev.raiz){
        // Ao chegar no diretório raiz retorna uma / 
        char* end  = (char*)malloc(2);
        strcpy(end, "/");
        return end;
    }
    else{
        //obtem o proximo recursivamente
        char * recived = getCaminho(getCabecalho(dir).dir);
        // obtem tamanho
        int sizeRecived = strlen(recived);
        // aloca tamanho + 1 '\0'
        char * caminho = (char*)malloc(sizeRecived + 1);
        // obtem o que veio
        strcpy(caminho, recived);
        // viber o ponteiro alocado na recursao anterior
        free(recived);
        // Quando não é o diretorio raiz obtem o nome do diretorio que esta sendo percorrido
        char dirName[26];
        strcpy(dirName, getCabecalho(dir).nome);
        int tam = strlen(dirName);
        // realoca para guardar o quie ja tinha + o que vai receber + '/' + '\0'
        caminho = (char*)realloc(caminho, sizeRecived + 2 + tam);
        // Concatena o caminho com o diretorio percorrido e adiciona uma /
        return strcat(strcat(caminho, dirName), "/");
    }
}

unsigned int fileFromName(char* nome, struct Linha_dir* lista, int tam){
    struct Linha_dir* aux = lista;
    for(int i = 0; i < tam; i++){
        // Caso encontre o arquivo ou diretorio procurado retorna seu endereco
        #ifdef DEBUG
            printf("%d: %s - %s:%d \n", i, nome, aux->nome, aux->endereco);
        #endif
        if(strcmp(nome, aux->nome) == 0){
            return aux->endereco;
        }
        aux++;
    }
    // Caso não foi encontrado retorna 0
    return 0;
}

unsigned int fileContainsName(char* nome, struct Linha_dir* lista, int tam){
    struct Linha_dir* aux = lista;
    for(int i = 0; i < tam; i++){
        // Caso encontre o arquivo ou diretorio procurado retorna seu endereco
        #ifdef DEBUG
            printf("%d: %s - %s:%d \n", i, nome, aux->nome, aux->endereco);
        #endif
        if(strstr(aux->nome, nome) != NULL){
            return aux->endereco;
        }
        aux++;
    }
    // Caso não foi encontrado retorna 0
    return 0;
}

int justDots(const char* string){
    int i = 0;
    while(string[i] != '\0'){
        if(string[i] != '.'){
            //printf("DEBUG: não são somente pontos, encontrado: %c\n", string[i]);
            return 0;            
        }
        i++;
    }
    //printf("DEBUG: somente pontos\n");
    return 1;
}

unsigned int pathToFileId(const char* path){
    char *str = strdup(path);
    
    unsigned int entrys_count = 0;
    struct Linha_dir* c_dir_entrys = NULL;

    /* get the first token */
   char* token = strtok(str, "/");
   unsigned int c_file;
   if(str[0] == '/'){
    c_file = dev.raiz;
   }
   else{
    c_file = dev.dirAtual;
   }
   

   /* walk through other tokens */
   while( token != NULL ) {
      //printf( "%s\n", token );
      entrys_count = getDirCount(c_file);
      c_dir_entrys = lerDiretorio(c_file);
      if(strcmp(token, ".") != 0){
        if(justDots(token)){
            unsigned int dir_aux = c_file;
            int i = 0;
            while(token[i] != '\0'){
                if(i != 0){
                    dir_aux = getCabecalho(dir_aux).dir;
                }
                i++;
            }
            c_file = dir_aux;
        }
        c_file = fileFromName(token, c_dir_entrys, entrys_count);
      }
      token = strtok(NULL, "/");
   }
    free(str);
    return c_file;
}

char* getContent(char* command){    
    //unsigned int entrys_count = 0;
    //struct Linha_dir* c_dir_entrys = NULL;

    /* get the first token */
    char* token = strtok(command, "\"");
    token = strtok(NULL, "\"");

    // funciona no caso de uso do sistema, não para uma estrada genérica
    char *content = strdup(token);
    if(content != NULL){
        token = strtok(NULL, "\"");
        if(token == NULL){
            return NULL;
        }
        else{
            return content;
        }
    }
    else{
        return NULL;
    }
}

int gravarCabecalho(unsigned int arquivo, struct Arquivo cabecalho){
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(arquivo, buffer);
    memcpy(buffer, &cabecalho, sizeof(struct Arquivo));
    escrever_bloco(arquivo, buffer);
    return 1; 
}

int createTagInFile(char** listOfTags, unsigned int file){

    // Cria nova estrutura
    struct FileTags fileTags;
    fileTags.proximoBloco = 0;
    fileTags.quantidadeAqui = 1;
    // le bloco incial do arquivo para buffer
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(file, buffer);
    // adiciona a estrutura de tag após o cabecalho
    memcpy(buffer + sizeof(struct Arquivo), &fileTags, sizeof(struct FileTags));
    // adiciona a primeira tag ao buffer após a estrutura de tag
    memcpy(buffer + sizeof(struct Arquivo) + sizeof(struct FileTags), listOfTags[0], 24);
    // grava o buffer
    escrever_bloco(file, buffer);
    free(buffer);

    //printf("Cabecalho lido com sucesso!\n");
    return 1;
}

int gravaTagsBloco(char** listOfTags, uint quantity, uint block, uint posicaoAtual){

    //obtem estrutura inical do bloco
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(block, buffer);
    void *ptr = buffer;
    struct FileTags fileTags;
    memcpy(&fileTags, buffer, sizeof(struct FileTags));
    fileTags.quantidadeAqui = (dev.tamBloco - sizeof(struct FileTags))/24;
    // Se cabem mais ou o necessário necessário, é isso
    if(fileTags.quantidadeAqui >= quantity){
        fileTags.quantidadeAqui = quantity;
    }
    // se não vai para o outro bloco
    else{
        // se já existe um bloco reservado
        if(fileTags.proximoBloco != 0){
            //grava nele o resto 
            gravaTagsBloco(listOfTags, quantity - fileTags.quantidadeAqui, fileTags.proximoBloco, posicaoAtual + fileTags.quantidadeAqui);
        }
        // se não
        else{
            //aloca novo bloco
            //printf("[1043]Alocando novo bloco para aramazenar tags: ");
            fileTags.proximoBloco = obtem_bloco_vazio();
           // printf("%d\n", fileTags.proximoBloco);
            dev.tabela[fileTags.proximoBloco] = -1;
            escrever_blocos_tabela();
            formataBlocoTag(fileTags.proximoBloco);
            //esvreve as tags no proximo bloco
            gravaTagsBloco(listOfTags, quantity - fileTags.quantidadeAqui, fileTags.proximoBloco, posicaoAtual + fileTags.quantidadeAqui);
        }
    }
    //escreve as tags
    memcpy(buffer, &fileTags, sizeof(struct FileTags));
    ptr += sizeof(struct FileTags);
    for(uint i = posicaoAtual; i< (posicaoAtual + fileTags.quantidadeAqui); i++){
        memcpy(ptr, listOfTags[i], 24);
        ptr+= 24;
    }
    escrever_bloco(block, buffer);
    free(buffer);
    return 1;
}

int formataBlocoTag(unsigned int block){
    // le bloco
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(block, buffer);
    // inicializa estrutura
    struct FileTags fileTags;
    fileTags.quantidadeAqui = 0;
    fileTags.proximoBloco = 0;
    //grava no bloco
    memcpy(buffer, &fileTags, sizeof(struct FileTags));
    escrever_bloco(block, buffer);
    free(buffer);
    return 1;
}

int gravaTags(char** listOfTags, uint quantity, uint file){

    // obtem o bloco
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(file, buffer);
    void *ptr = buffer;
    struct FileTags fileTags;
    memcpy(&fileTags, buffer + sizeof(struct Arquivo), sizeof(struct FileTags));
    // veririca quantas cabem no bloco
    fileTags.quantidadeAqui = (dev.tamBloco - sizeof(struct Arquivo) - sizeof(struct FileTags))/24;

    //printf("É possível gravar %d tags nesse bloco.\n", fileTags.quantidadeAqui);

    // Se cabem mais ou o necessário necessário, é isso
    if(fileTags.quantidadeAqui >= quantity){
        fileTags.quantidadeAqui = quantity;
    }
    // se não vai para o outro bloco
    else{
        // se já existe um bloco reservado
        if(fileTags.proximoBloco != 0){
            //grava nele o resto 
            gravaTagsBloco(listOfTags, quantity - fileTags.quantidadeAqui, fileTags.proximoBloco,  fileTags.quantidadeAqui);
        }
        // se não
        else{
            //aloca novo bloco
            //printf("[1106]Alocando novo bloco para aramazenar tags: ");
            fileTags.proximoBloco = obtem_bloco_vazio();
            //printf("%d\n", fileTags.proximoBloco);
            dev.tabela[fileTags.proximoBloco] = -1;
            escrever_blocos_tabela();
            formataBlocoTag(fileTags.proximoBloco);
            //esvreve as tags no proximo bloco
            gravaTagsBloco(listOfTags, quantity - fileTags.quantidadeAqui, fileTags.proximoBloco,  fileTags.quantidadeAqui);

        }
    }
    //printf("%d serão gravadas aqui\n", fileTags.quantidadeAqui);
    //escreve as tags
    memcpy(buffer + sizeof(struct Arquivo), &fileTags, sizeof(struct FileTags));
    ptr += (sizeof(struct Arquivo) + sizeof(struct FileTags));
    for(uint i =0; i<fileTags.quantidadeAqui; i++){
        //printf("Gravando \"%s\" na memória\n", listOfTags[i]);
        memcpy(ptr, listOfTags[i], 24 * sizeof(char));
        free(listOfTags[i]);
        ptr+= 24;
    }
    escrever_bloco(file, buffer);
    free(buffer);

    //printf("Cabecalho lido com sucesso!\n");
    return 1;
}


int getTagsFromBlock(char** listOfTags, uint block, uint posicaoAtual){

//obtem estrutura inical do bloco
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(block, buffer);
    void *ptr = buffer;
    struct FileTags fileTags;
    memcpy(&fileTags, buffer, sizeof(struct FileTags));

    if(fileTags.proximoBloco != 0){
        //grava nele o resto 
        //printf("[1146]Obtendo tags do próximo bloco: %d\n", fileTags.proximoBloco);
        getTagsFromBlock(listOfTags, fileTags.proximoBloco, posicaoAtual + fileTags.quantidadeAqui);
    }
    //le as tags
    ptr += sizeof(struct FileTags);
    for(uint i = posicaoAtual; i< (posicaoAtual + fileTags.quantidadeAqui); i++){
        memcpy(listOfTags[i], ptr, 24);
        ptr+= 24;
    }
    free(buffer);
    return 1;
}


char** getTagsFromFile(unsigned int file, unsigned int * count){

    // le bloco incial do arquivo para buffer
    void *buffer = malloc(dev.tamBloco);
    void *ptr = buffer;
    ler_bloco(file, buffer);
    // obtem cabeçalho
    struct Arquivo header;
    memcpy(&header, buffer, sizeof(struct Arquivo));
    // alloca ponteiro para guardar lista de tags
    char ** tagList = (char **)malloc(header.count_tags * sizeof(char*));
    for(uint i =0; i<header.count_tags; i++){
        tagList[i] = (char*)malloc(24 * sizeof(char));
    }
    (*count) = header.count_tags;
    // avança no buffer
    buffer += sizeof(struct Arquivo);
    //obtem quantidade de tags no bloco
    struct FileTags fileTags;
    memcpy(&fileTags, buffer, sizeof(struct FileTags));

    // se existem tags em outros blocos, lê
    if(fileTags.proximoBloco != 0){
        //grava nele o resto 
        //printf("[1184]Obtendo tags do próximo bloco: %d\n", fileTags.proximoBloco);
        getTagsFromBlock(tagList, fileTags.proximoBloco, fileTags.quantidadeAqui);
    }

    // avança no buffer
    buffer += sizeof(struct FileTags);
    // obtem as tags gravadas no bloco incial
    for(uint i = 0; i<fileTags.quantidadeAqui; i++){
        strcpy(tagList[i], buffer);
        buffer += 24;
    }
    free(ptr);
    return tagList;
}

int clearTagsFromBlock(unsigned int block){
    //printf("limpando tags do bloco: %d", block);
    // le bloco incial do arquivo para buffer
    void *buffer = malloc(dev.tamBloco);
    //void *ptr = buffer;
    ler_bloco(block, buffer);
    //obtem estrutura de tags no bloco
    struct FileTags fileTags;
    memcpy(&fileTags, buffer, sizeof(struct FileTags));
    // limpa proximo bloco se tiver
    if(fileTags.proximoBloco != 0){
        clearTagsFromBlock(fileTags.proximoBloco);
    }
    // limpa bloco
    fileTags.proximoBloco = 0;
    fileTags.quantidadeAqui = 0;
    // salva limpeza
    memcpy(buffer, &fileTags, sizeof(struct FileTags));
    // libera o bloco
    dev.tabela[fileTags.proximoBloco] = 0;
    escrever_blocos_tabela();
    escrever_bloco(block, buffer);
    free(buffer);

    return 1;
}

int clearTagsFromFile(unsigned int file){

    // ARVORE
    unsigned int count;
    char** list = getTagsFromFile(file, &count);
    for(uint i =0; i<count ; i++){
        removeFileFromTag(&tagTree, file, list[i]);
        free(list[i]);
    }
    free(list);
    gravarTags();
    

    // ARQUIVO

    // le bloco incial do arquivo para buffer
    void *buffer = malloc(dev.tamBloco);
    //void *ptr = buffer;
    ler_bloco(file, buffer);
    // obtem header 
    struct Arquivo header;
    memcpy(&header, buffer, sizeof(struct Arquivo));
    header.count_tags = 0;
    memcpy(buffer , &header, sizeof(struct Arquivo));
    //obtem estrutura de tags no bloco
    struct FileTags fileTags;
    memcpy(&fileTags, buffer + sizeof(struct Arquivo), sizeof(struct FileTags));
    if(fileTags.proximoBloco != 0){
        clearTagsFromBlock(fileTags.proximoBloco);
    }
    fileTags.proximoBloco = 0;
    fileTags.quantidadeAqui = 0;
    memcpy(buffer + sizeof(struct Arquivo), &fileTags, sizeof(struct FileTags));

    escrever_bloco(file, buffer);
    free(buffer);
    return 1;
}

int addTagToFile(uint file, char* tagName){
    // ÁRVORE
    addFileToTag(&tagTree, file, tagName);
    gravarTags();
    //gravaEstruturaDeTag(dev.tagStructure);

    // ARQUIVO 
    struct Arquivo header = getCabecalho(file);

    header.count_tags++;
    uint count;
    //printf("adicionando nova tag: novo total: %d\n", header.count_tags);

    // se não existiam tags antes cria a nova estrutura 
    if(header.count_tags == 1){
        //printf("Criando estrutura de tag...\n");
        char ** listOfTags = (char**)(malloc(1 * sizeof(char*)));
        listOfTags[0] = malloc(24 * sizeof(char));
        strcpy(listOfTags[0], tagName);
        createTagInFile(listOfTags, file);
        free(listOfTags);
    }
    // se não, aumenta o tamanho da estrutura
    else{
        char ** listOfTags = getTagsFromFile(file, &count);
        //printf("tags existentes:\n");
        //for(uint i =0; i<count ; i++){
        //    printf("%s\n", listOfTags[i]);
        //}
        count++;
        listOfTags = (char **)realloc(listOfTags, count * sizeof(char *));
        listOfTags[count - 1] = malloc(24 * sizeof(char));
        strcpy(listOfTags[count - 1], tagName);
        //printf("Adicionando nova tag: %s\n", listOfTags[count - 1]);
        gravaTags(listOfTags, count, file);
        free(listOfTags);
        //printf("tags gravadas, novo total atualizado: %d\n", count);
        header.count_tags = count;
    }
    
    
    gravarCabecalho(file, header);
    return 1;
}
int removeTagFromFile(uint file, char* tagName){

    // ARVORE
    removeFileFromTag(&tagTree, file, tagName);
    gravarTags();
    //gravaEstruturaDeTag(dev.tagStructure);

    // ARQUIVO
    unsigned int count;
    uint indexToremove = 0;
    bool found = false;
    // Pega lista atual
    char** list = getTagsFromFile(file, &count);
    char** newList;

    for(uint i =0; i<count ; i++){
        if(strcmp(list[i], tagName) == 0){
            found = true;
            indexToremove = i;
            break;
        }
    }
    if(found){
        newList = (char**) malloc((count - 1) * sizeof(char*));
        uint j = 0;
        for(uint i = 0; i<count; i++){
            if(i != indexToremove){
                newList[j] = list[i];
                j++;
            }
        }
    }
    else{
        free(list);
        return -1;
    }

    // atualiza a quantidade de tags no header
    struct Arquivo header = getCabecalho(file);
    header.count_tags--;
    void *buffer = malloc(dev.tamBloco);
    ler_bloco(file, buffer);
    memcpy(buffer, &header, sizeof(struct Arquivo));
    escrever_bloco(file, buffer);

    // grava lista de tags atualizadas
    gravaTags(newList, count-1, file);

    free(buffer);
    free(list);
    free(newList);
    return 1;
}

int quitFS(){
    // limpar os buffers
    if(tagStructure.buffers.lists != NULL){
        for(uint i = 0; i < tagStructure.buffers.lists->count; i++){
            //printf("LIMPANDO LISTA %d: ", i);
            for(uint j = 0; j < tagStructure.buffers.lists->counts[i]; j++){
                 //printf("%d ", tagStructure.buffers.lists->filesLists[i][j]);
            }
            //printf("\n");
            if(tagStructure.buffers.lists->counts[i] > 0){
                free(tagStructure.buffers.lists->filesLists[i]);
            }
            else{
             //   printf("VAZIO< NÃO LIMPOU\n");
            }
        }
        free(tagStructure.buffers.lists->counts);
        free(tagStructure.buffers.tree);
        free(tagStructure.buffers.lists);
    }
    free(dev.tabela);
    sem_destroy(&semaforo);
    return 1;
}