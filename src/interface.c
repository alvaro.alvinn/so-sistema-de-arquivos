#include "../include/interface.h"
#include <time.h> 

#ifdef _WIN32
    #include <windows.h> // notice this! you need it! (windows)
    #include <locale.h>
    #define WIN32_LEAN_AND_MEAN
    #define _UNICODE
    #include <windows.h>
    #define sleep(x) Sleep(x)
    void clrscr()
{
  HANDLE                     hStdOut;
  CONSOLE_SCREEN_BUFFER_INFO csbi;
  DWORD                      count;
  DWORD                      cellCount;
  COORD                      homeCoords = { 0, 0 };

  hStdOut = GetStdHandle( STD_OUTPUT_HANDLE );
  if (hStdOut == INVALID_HANDLE_VALUE) return;

  /* Get the number of cells in the current buffer */
  if (!GetConsoleScreenBufferInfo( hStdOut, &csbi )) return;
  cellCount = csbi.dwSize.X *csbi.dwSize.Y;

  /* Fill the entire buffer with spaces */
  if (!FillConsoleOutputCharacter(
    hStdOut,
    (TCHAR) ' ',
    cellCount,
    homeCoords,
    &count
    )) return;

  /* Fill the entire buffer with the current colors and attributes */
  if (!FillConsoleOutputAttribute(
    hStdOut,
    csbi.wAttributes,
    cellCount,
    homeCoords,
    &count
    )) return;

  /* Move the cursor home */
  SetConsoleCursorPosition( hStdOut, homeCoords );
}
#else
    #include <unistd.h> 
    #define clrscr() printf("\e[1;1H\e[2J")
#endif

void cleanInputBuffer(){
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

char inputBuffer[__INT16_MAX__];

int init(int argc, char *argv[]){
    sem_init(&semaforo, 0, 1); // Inicializa o semáforo com valor 1 (binário)
    if(argc == 4 && (strcmp(argv[1], "-f")==0)){
        printf("formatando...\n");
        fs_formatar(argv[3], atoi(argv[2]));
        printf("%s formatado com sucesso!\n", argv[3]);
        return 1;
    }
    if(argc == 3){
        printf("para formatar é preciso passar também o tamanho de bloco desejado\n");
        return -1;
    }
    if(argc == 2 && (strcmp(argv[1], "-f")==0)){
        printf("para formatar é preciso passar o nome do dispositivo "
               "e também o tamanho de bloco desejado\n");
        return -1;
    }   
    if(argc == 1){
        printf("Por favor inicie o programa indicado o dispositivo que deseja abrir, ou formatar\n");
        return -1;
    }
    printf("Abrindo dispositivo...\n");
    dev_abrir(argv[1]);
    printf("Extraindo configuração do dispositivo...\n");
    if(fs_carregar_config() == -1){
        return -1;
    }
    printf("SISTEMA RECUPERADO COM SUCESSO!\n");
    printf("Para ajuda digite 'help', para sair digite 'exit'!\n");

    return 0;
    
}

    int tam_dir = 0;
    struct Linha_dir * lista;

unsigned int fileFromPathGeneral(char* path){
    unsigned int dir_aux;
    if(strstr(path, "/") == NULL){
        if(justDots(path)){
            dir_aux = dev.dirAtual;
            int i = 0;
            while(path[i] != '\0'){
                if(i != 0){
                    dir_aux = getCabecalho(dir_aux).dir;
                }
                i++;
            }
        }
        else{
            dir_aux = fileFromName(path, lista, tam_dir);
        }
    }
    else{
        dir_aux = pathToFileId(path);
    }
    return dir_aux;
}

int mkdir(int argc, char *argv[]){
    criaDir(argv[1], obtem_bloco_vazio(), dev.dirAtual);
    return 1;
}

int ls(int argc, char *argv[]){
    struct Linha_dir* toFree = printDir(dev.dirAtual);
    free(toFree);
    return 1;
}

int touch(int argc, char *argv[]){
    if(argc != 2){
        printf("esperado um argumento: touch <nome do arquivo>\n");
        return -1;
    }
    criaArquivoText(argv[1], obtem_bloco_vazio(), dev.dirAtual);
    return 1;
}

int findInDir(uint dir, char* name){
    // Start measuring time
    //struct timespec begin, end; 
    //clock_gettime(CLOCK_REALTIME, &begin);
    bool contemDir = false;
    uint tam_dir = getDirCount(dir);
    if( tam_dir > 0){
        struct Linha_dir * lista = (struct Linha_dir *)malloc(tam_dir * sizeof(struct Linha_dir));
        //printf("%d\n", dir);
        lista = lerDiretorio(dir);
        //for(uint i =0; i<tam_dir; i++){
            //printf(" - %s\n", lista[i].nome);
        //}
        unsigned int file = fileContainsName(name, lista, tam_dir);
        if(file != 0){

            char * caminho = getCaminho(file);
            caminho[strlen(caminho)-1] = '\0';
            sem_wait(&semaforo); // Aguarda disponibilidade do semáforo
                printf("%s\n", caminho);
            sem_post(&semaforo); // Libera o semáforo
            //printf("encontou!\n");
            free(lista);
            return 1;
        }
        else{
            unsigned int i = 0;
            while (i < tam_dir - 1){
                {   
                    // Caso seja um diretório busca
                    if(getCabecalho(lista[i].endereco).is_dir){
                        //printf("indo verificar diretório %s de endereço %d\n", lista[i].nome, lista[i].endereco);
                        uint id = lista[i].endereco;
                        findInDir(id, name);
                        contemDir = true;
                    }
                }
                i++;
            }
            // Caso seja um diretório busca
            if(getCabecalho(lista[i].endereco).is_dir){
                //printf("indo verificar diretório %s\n", lista[i].nome);
                uint id = lista[i].endereco;
                findInDir( id, name);
                contemDir = true;
            }
            free(lista);
        }
    }
    // Stop measuring time and calculate the elapsed time
    //clock_gettime(CLOCK_REALTIME, &end);
    //long seconds = end.tv_sec - begin.tv_sec;
    //long nanoseconds = end.tv_nsec - begin.tv_nsec;
    //double elapsed = seconds + nanoseconds*1e-9;
    
    ///printf("Result: %.20f\n", sum);
    
    //printf("Time measured: %.10f seconds - full.\n", elapsed);

    return contemDir;
}

int nameFind(int argc, char * argv[]){
    if(argc != 2){
        printf("esperado um argumento: find <termo para busca>\n");
        return -1;
    }
    // Start measuring time
    //struct timespec begin, end; 
    //clock_gettime(CLOCK_REALTIME, &begin);
    findInDir(dev.dirAtual, argv[1]);

        // Stop measuring time and calculate the elapsed time
    //clock_gettime(CLOCK_REALTIME, &end);
    //long seconds = end.tv_sec - begin.tv_sec;
    //long nanoseconds = end.tv_nsec - begin.tv_nsec;
    //double elapsed = seconds + nanoseconds*1e-9;
    
    ///printf("Result: %.20f\n", sum);
    
    //printf("Time measured: %.10f seconds - FULL!\n", elapsed);
    return 1;
}

void * threadNameFind(void *arg){
    struct ThreadInput* threadArg = (struct ThreadInput*)arg;
    nameFind(threadArg->argc, threadArg->argv);
}



int tagfind(int argc, char * argv[], bool sinalize){
    if(argc != 2){
        printf("esperado um argumento: tagfind <tag para busca>\n");
        return -1;
    }
    Tag* tagInfo = getTag(&tagTree, argv[1]);
    if(tagInfo != NULL){
        ListNode* filesInList = tagInfo->root;
        while (filesInList != NULL)
        {
            if(filesInList->file == 0){
                printf("ERRO NA LISTA DE ARQUIVOS DA TAG\n");
                break;
            }
            char * caminho = getCaminho(filesInList->file);
            caminho[strlen(caminho)-1] = '\0';
                if(sinalize){
                    sem_wait(&semaforo); // Aguarda disponibilidade do semáforo
                    printf("%s (TAG)\n", caminho);
                    sem_post(&semaforo); // Libera o semáforo
                }
                else{
                    sem_wait(&semaforo); // Aguarda disponibilidade do semáforo
                    printf("%s\n", caminho);
                    sem_post(&semaforo); // Libera o semáforo
                }
            
            free(caminho);
            filesInList = filesInList->next;
        }
        
    }
    else{
        printf("Nenhum arquivo com a tag solicitada: %s\n", argv[1]);
        return -1;
    }
    return 1;
}

void * threadTagFind(void *arg){
    struct ThreadInput* threadArg = (struct ThreadInput*)arg;
    tagfind(threadArg->argc, threadArg->argv, true);
}

int find(int argc, char * argv[]){

    pthread_t threads[2];

    // create argument
    struct ThreadInput* threadArg = (struct ThreadInput*)malloc(sizeof(struct ThreadInput)) ;
    threadArg->argc = argc;
    threadArg->argv = (char**)malloc(argc * sizeof(char*));
    for(int i = 0; i<argc; i++){
        threadArg->argv[i] = argv[i];
    }
    

    // call functions in threads
    pthread_create(&threads[0], NULL, threadTagFind, threadArg);
    pthread_create(&threads[1], NULL, threadNameFind, threadArg);

    // wait for threads
    pthread_join(threads[0], NULL);
    pthread_join(threads[1], NULL);

    free(threadArg->argv);
    free(threadArg);
    return 1;
}

int cd(int argc, char *argv[]){
    if(argc != 2){
        printf("esperado um argumento: cd <caminho>\n");
        return -1;
    }
    unsigned int dir_aux = fileFromPathGeneral(argv[1]);
    if(getCabecalho(dir_aux).is_dir){
        dev.dirAtual = dir_aux;
        return 1;
    }
    return -1;
}

int rm(int argc, char *argv[]){
    if(argc != 2){
        printf("esperado um argumento: rm <caminho>\n");
        return -1;
    }
    unsigned int file_or_dir_aux = fileFromPathGeneral(argv[1]);
    if(file_or_dir_aux == 0){
        return -1;
    }
    if(file_or_dir_aux == dev.raiz){
        printf("Não é possível deletar o diretório raiz!\n");
        return -1;
    }
    if(getCabecalho(file_or_dir_aux).is_dir){
        return delDir(file_or_dir_aux);  
    }
    else{
        return delArquivo(file_or_dir_aux);
    }
}

int cat(int argc, char *argv[]){
    unsigned int arquivo_aux = fileFromPathGeneral(argv[1]);
    // Caso não seja encontrado
    if(arquivo_aux == 0){
        return -1;
    }
    if(!getCabecalho(arquivo_aux).is_dir){
        printArquivoText(arquivo_aux);
    }
    return 1;
}

int echo(int argc, char *argv[], char* fullCommand){
    // obter o text oque deve ser escrito
    char* content = getContent(fullCommand);
    if(content == NULL){
        return -1;
    }
    unsigned int arquivo_aux = fileFromPathGeneral(argv[argc-1]);
    escreveArquivoText(content, strlen(content), arquivo_aux);
    free(content);
    return 1;
}

int addtag(int argc, char *argv[]){
    if(argc != 3){
        printf("esperado dois argumentos: addtag <tag> <caminho>\n");
        return -1;
    }
    uint file = fileFromPathGeneral(argv[2]);
    if(file != 0){
        addTagToFile(file, argv[1]);
        return 1;
    }
    return -1;   

}

int rmtag(int argc, char *argv[]){
    if(argc != 3){
        printf("esperado dois argumentos: rmtag <tag> <caminho>\n");
        return -1;
    }
    uint file = fileFromPathGeneral(argv[2]);
    if(file != 0){
        removeTagFromFile(file, argv[1]);
        return 1;
    }
    return -1;

}

int ltags(int argc, char *argv[]){
    if(argc != 2){
        printf("esperado um argumento: ltags <caminho>\n");
        return -1;
    }
    unsigned int count;
    uint file = fileFromPathGeneral(argv[1]);
    if(file != 0){
        char** list = getTagsFromFile(file, &count);
        for(uint i =0; i<count ; i++){
            printf("%s\n", list[i]);
        }
        free(list);
        return 1;
    }
    return -1;
}

int ctags(int argc, char *argv[]){
    if(argc != 2){
        printf("esperado um argumento: ctags <caminho>\n");
        return -1;
    }
    uint file = fileFromPathGeneral(argv[1]);
    if(file != 0){
        clearTagsFromFile(fileFromPathGeneral(argv[1]));
        return 1;
    }
    return -1;
}

int printTags(){
    if(tagTree != NULL){
        //tagTree = getTreeFromBuffers(&tagStructure.buffers);

        printf("Número de tags: %d\n", tagStructure.buffers.lists->count);    
        printf("listas:\n");
        for(uint i = 0; i<tagStructure.buffers.lists->count; i++){
            printf("%d: ", i);
            for(uint j = 0; j<tagStructure.buffers.lists->counts[i]; j++){
                printf("%d ", tagStructure.buffers.lists->filesLists[i][j]);
            }
            printf("\n");
        }
        printf("ARVORE:\n");
        printTree(&tagTree);
    }
    return 1;
}

void clear(){
    clrscr();
}

void test(int argc, char *argv[]){
    printf("%d\n", fileFromPathGeneral(argv[1]));
}

int run(int argc, char *argv[]){
    int initValue = init(argc, argv);
    if (initValue != 0){
        return initValue;
    }

    bool quit = false;
    while(!quit){
        tam_dir = getDirCount(dev.dirAtual);
        lista = (struct Linha_dir *)malloc(tam_dir * sizeof(struct Linha_dir));
        lista = lerDiretorio(dev.dirAtual);

        printf("%s$ ", getCaminho(dev.dirAtual));
        fgets(inputBuffer,__INT16_MAX__, stdin);
        char tmp_input_buffer[__INT16_MAX__];
        strcpy(tmp_input_buffer, inputBuffer);
        char echoBuffer[__INT16_MAX__];
        strcpy(echoBuffer, inputBuffer);
        char * token;
        int local_argc = 0;
        //size_t tokens_size = 0;
        token = strtok (tmp_input_buffer," \n");
        while (token != NULL)
        {
            local_argc++;
            //printf ("%s\n",token);
            token = strtok(NULL, " \n");
        }
        //printf("local_argc: %d\n", local_argc);
        char **local_argv = realloc(local_argv, local_argc * sizeof(char*));
        int i = 0;
        token = NULL;
        token = strtok (inputBuffer," \n");
        //printf("local_argv[%d] = %s\n", i, token);
        if(token != NULL){
            local_argv[i] = strdup(token);
            while (token != NULL)
            {
                //printf ("%s\n",token);
                token = strtok(NULL, " \n");
                i++;
                if(token != NULL){
                    //printf("local_argv[%d] = %s\n", i, token);
                    local_argv[i] = strdup(token);
                }
                
            }

            if (strcmp(local_argv[0], "exit") == 0 || strcmp(local_argv[0], "q") == 0) 
            {
                quit = true;
                free(local_argv);
                quitFS();
            } 
            else if (strcmp(local_argv[0], "help") == 0)
            {   
                printf("Comandos disponíveis:\n\n");
                printf("q, exit  - Sair\n");
                printf("ls       - Listar diretório atual\n");
                printf("cd       - Navegar até diretório selecionado\n");
                printf("mkdir    - Cria novo diretório\n");
                printf("touch    - Cria um arquivo\n");
                printf("find     - Encontrar arquivos/diretórios com base em tags e nomes\n");
                printf("namefind - Encontrar arquivos/diretórios com base no nome\n");
                printf("rm       - Remove arquivo ou diretório\n");
                printf("cat      - Mostra o conteúdo do arquivo\n");
                printf("echo     - Escreve no arquivo\n");
                printf("clear    - Limpa a tela\n");
                printf("addtag   - Adiciona tag ao arquivo\n");
                printf("rmtag    - Remove tag do arquivo\n");
                printf("ltags    - Lista as tags associadas com o arquivo\n");
                printf("ctags    - Limpa as tags de um arquivo\n");
                printf("tagfind  - Encontrar arquivos/diretórios com base em tag\n");
                printf("\n");
            }
            else if (strcmp(local_argv[0], "ls") == 0)
            {
                ls(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "cd") == 0)
            {
                cd(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "mkdir") == 0)
            {
                mkdir(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "touch") == 0)
            {
                touch(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "namefind") == 0)
            {
                nameFind(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "rm") == 0)
            {
                rm(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "cat") == 0)
            {
                cat(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "echo") == 0)
            {
                echo(local_argc, local_argv, echoBuffer);
            }
            else if (strcmp(local_argv[0], "clear") == 0)
            {
                clear();
            }
            else if (strcmp(local_argv[0], "ltags") == 0)
            {
                ltags(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "ctags") == 0)
            {
                ctags(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "addtag") == 0)
            {
                addtag(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "rmtag") == 0)
            {
                rmtag(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "tagfind") == 0)
            {
                tagfind(local_argc, local_argv, false);
            }
            else if (strcmp(local_argv[0], "find") == 0)
            {
                find(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "test") == 0)
            {
                test(local_argc, local_argv);
            }
            else if (strcmp(local_argv[0], "printTags") == 0)
            {
                printTags();
            }
            else
            {
                printf("comando não reconhecido! digite help para obter os comandos disponíveis!\n");
            }
        }

    }
    return 1;
}

int runOld(int argc, char *argv[])
{   
    // Para formatar executar com -f
    if(argc == 4 && (strcmp(argv[1], "-f")==0)){
        printf("formatando...\n");
        fs_formatar(argv[3], atoi(argv[2]));
        printf("%s formatado com sucesso!\n", argv[3]);
        return 0;
    }
    if(argc == 3){
        printf("para formatar é preciso passar também o tamanho de bloco desejado\n");
        return -1;
    }
    if(argc == 2 && (strcmp(argv[1], "-f")==0)){
        printf("para formatar é preciso passar o nome do dispositivo "
               "e também o tamanho de bloco desejado\n");
        return -1;
    }   
    if(argc == 1){
        printf("Por favor inicie o programa indicado o dispositivo que deseja abrir, ou formatar\n");
        return -1;
    }
    printf("Abrindo dispositivo...\n");
    dev_abrir(argv[1]);
    printf("Extraindo configuração do dispositivo...\n");
    if(fs_carregar_config() == -1){
        return -1;
    }
    printf("SISTEMA RECUPERADO COM SUCESSO!\n");

    // Variaveis auxiliares utilizadas no switch
    char nome[26];
    //char c_aux;
    int tam;
    int dir_aux;
    int arquivo_aux;
    //int tam_dir;

    //struct Linha_dir * lista;
    // Cria um buffer para receber entrada
    char buffer[__INT16_MAX__];

    short int loop = false;
    while (loop !=9)
    {   
        tam_dir = getDirCount(dev.dirAtual);
        lista = (struct Linha_dir *)malloc(tam_dir * sizeof(struct Linha_dir));

        printf("%s %s\n", dev.nome, getCaminho(dev.dirAtual));
        lista = printDirOld(dev.dirAtual);
        printf("\n ------------------------------------------------------ \n");
        printf("Escolha o processo desejado:\n");
        printf("[1] - Criar um Diretorio\n");
        printf("[2] - Criar um Arquivo\n");
        printf("[3] - Deletar um Diretorio\n");
        printf("[4] - Deletar um Arquivo\n");
        printf("[5] - Escrever em um Arquivo\n");
        printf("[6] - Acessar diretorio\n");
        printf("[7] - Ler arquivo\n");
        printf("[8] - Voltar diretorio\n");
        printf("[9] - Sair\n");
        loop = fgetc(stdin) - '0';
        cleanInputBuffer();

        switch (loop)
        {
        case 0: // Criar um Diretorio
            pathToFileId("/dir/livros/o_melhor_livro");
            break;
        case 1: // Criar um Diretorio
            printf("Digite o nome do diretorio:\n");
            fgets(nome, 26, stdin);

            tam = strlen(nome); // Total size of string
            nome[tam-1] = '\0';
            
            printf("Criando novo diretorio %s...\n", nome);
            criaDir(nome, obtem_bloco_vazio(), dev.dirAtual);
            printf("Ok\n");
            sleep(3);  
            // Limpa a tela 
            clrscr();
            break;
        case 2: // Criar um Arquivo
            printf("Digite o nome do arquivo:\n");
            fgets(nome, 26, stdin);

            tam = strlen(nome); // Total size of string
            nome[tam-1] = '\0';

            printf("Criando novo arquivo %s...\n", nome);
            criaArquivoText(nome, obtem_bloco_vazio(), dev.dirAtual);
            printf("Ok\n");
            sleep(3);  
            // Limpa a tela
            clrscr();
            break;

        case 3: // Deletar um Diretorio
            printf("Insira o nome do diretorio que você deseja apagar: \n");
            
            fgets(nome, 26, stdin);
            tam = strlen(nome); //Total size of string
            nome[tam-1] = '\0';
            dir_aux = fileFromName(nome, lista, tam_dir);
            // Caso não seja encontrado
            if(dir_aux == 0){
                printf("Diretorio digitado não encontrado no diretório atual!\n");
                sleep(3);
                clrscr();
                break;
            }
            //cleanInputBuffer();
            if(getCabecalho(dir_aux).is_dir){
                delDir(dir_aux);
                printf("Diretorio deletado\n");
            }
            else{
                printf("Identificador digitado não é referente a um diretorio!\n");
            }
            sleep(3);  
            // Limpa a tela
            clrscr();

            break;
        case 4: // Deletar um Arquivo
            printf("Insira o nome do arquivo que você deseja apagar: \n");
            
            fgets(nome, 26, stdin);
            tam = strlen(nome); //Total size of string
            nome[tam-1] = '\0';
            arquivo_aux = fileFromName(nome, lista, tam_dir);
            // Caso não seja encontrado
            if(arquivo_aux == 0){
                printf("Arquivo digitado não encontrado no diretório atual!\n");
                sleep(3);
                clrscr();
                break;
            }
            //cleanInputBuffer();
            if(!getCabecalho(arquivo_aux).is_dir){
                delArquivo(arquivo_aux);
                printf("Arquivo deletado\n");
            }
            else{
                printf("Identificador digitado não é referente a um arquivo!\n");
            }
            sleep(3);  
            // Limpa a tela
            clrscr();
            break;
        case 5: // Escrever em um Arquivo
            printf("Insira o nome do arquivo que você deseja escrever: \n");
            fgets(nome, 26, stdin);
            tam = strlen(nome); // Total size of string
            nome[tam-1] = '\0';
            arquivo_aux = fileFromName(nome, lista, tam_dir);
            // Caso não seja encontrado
            if(arquivo_aux == 0){
                printf("Arquivo digitado não encontrado no diretório atual!\n");
                sleep(3);
                clrscr();
                break;
            }
            //cleanInputBuffer();
            if(!getCabecalho(arquivo_aux).is_dir){
                printf("Digite o que gostaria de escrever no arquivo:\n");
		    memset(buffer, 0, __INT16_MAX__);
                fgets(buffer, __INT16_MAX__, stdin);


                // Pega o tamanmho da entrada digitada
                tam = strlen(buffer);
                if (tam > 0 && buffer[tam - 1] == '\n')
                buffer[tam - 1] = '\0';

                // Escreve a entrada no primeiro arquivo
                escreveArquivoText(buffer, tam, arquivo_aux);

                printf("Arquivo escrito no disco.\n");
            }
            else{
                printf("Identificador digitado não é referente a um arquivo!\n");
                printf("Arquivo: %d - %s\n e diretorio: %d", dir_aux, getCabecalho(dir_aux).nome, (int)getCabecalho(dir_aux).is_dir);
            }
            sleep(3);  
            // Limpa a tela
            clrscr();
            break;
        case 6: // Acessar diretorio
            printf("Insira o nome do diretorio que você deseja acessar: \n");
            fgets(nome, 26, stdin);
            tam = strlen(nome); //Total size of string
            nome[tam-1] = '\0';
            dir_aux = fileFromName(nome, lista, tam_dir);
            // Caso não seja encontrado
            if(dir_aux == 0){
                printf("Diretorio digitado não encontrado no diretório atual!\n");
                sleep(3);
                clrscr();
                break;
            }
            //cleanInputBuffer();
            if(getCabecalho(dir_aux).is_dir){
                dev.dirAtual = dir_aux;
                printf("Ok\n");
            }
            else{
                printf("Identificador digitado não é referente a um diretorio!\n");
                sleep(3);
            }
            // Limpa a tela
            clrscr();
            break;
        
        case 7: // Ler arquivo
            printf("Insira o nome do arquivo que você deseja ler: \n");
            fgets(nome, 26, stdin);
            tam = strlen(nome); //Total size of string
            nome[tam-1] = '\0';
            arquivo_aux = fileFromName(nome, lista, tam_dir);
            // Caso não seja encontrado
            if(arquivo_aux == 0){
                printf("Arquivo digitado não encontrado no diretório atual!\n");
                sleep(3);
                clrscr();
                break;
            }
            if(!getCabecalho(arquivo_aux).is_dir){
                clrscr();  
                printArquivoText(arquivo_aux);
                printf("\nDigite qualquer tecla para continuar\n");
                fgetc(stdin);
            }
            else{
                printf("Identificador digitado não é referente a um arquivo!\n");
            }
            // Limpa a tela
            clrscr();            
            break;
        case 8: // Voltar diretorio
            if(dev.dirAtual != dev.raiz){
                dev.dirAtual = getCabecalho(dev.dirAtual).dir;
                // Limpa a tela
                clrscr();
            }
            else{
                printf("Você ja esta no diretorio raiz\n");
                sleep(3);
                // Limpa a tela
                clrscr();
            }
            break;
        case 9: // Sair
            
            break;
        default:
            printf("Por favor digite uma opcao valida!\n");
            sleep(3);  
            // Limpa a tela
            clrscr();
            break;
        }
    }

    dev_fechar();
    return 0;
}