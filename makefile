prefix = .
bindir = $(prefix)/
builddir = $(prefix)/build/
includedir = $(prefix)/include/
srcdir = $(prefix)/src/
testdir = $(prefix)/test/
INCLUDES = -I$(includedir)/

CCFLAGS = -g -Wextra

#OBJS = $(builddir)/dispositivoDeBloco.o $(builddir)/sistemaDeArquivos.o $(builddir)/interface.o
#
#
#
all:  main
#
#sistemaDeArquivos: dispositivoDeBloco
#	gcc $(CCFLAGS) -c sistemaDeArquivos.c -o $(builddir)/sistemaDeArquivos.o
#
#dispositivoDeBloco:
#	gcc $(CCFLAGS) -c dispositivoDeBloco.c -o $(builddir)/dispositivoDeBloco.o
#
#interface: sistemaDeArquivos 
#	gcc $(CCFLAGS) -c interface.c -o $(builddir)/interface.o
#
# Cria um dispositivo virtual vazio de 10Mb
data:
	truncate -s 10M $(bindir)disk.data

#fuse: dispositivoDeBloco sistemaDeArquivos
#	gcc $(CCFLAGS) $(OBJS) fuse_main.c -lfuse3 -o fs_fuse 
main:
	gcc $(CCFLAGS) $(srcdir)main.c -lm -lpthread -pthread -o fs

test: dispositivoDeBloco  sistemaDeArquivos
	gcc $(CCFLAGS) $(OBJS) $(testdir)test.c -o fs_test

clean:
	rm -f *.o
	rm -f $(builddir)*.o 
	rm -f *.out
	rm -f fs
	rm -f fs_test