#include <stdio.h>
#include <stdlib.h>
#include "dispositivoDeBloco.c"
#include "sistemaDeArquivos.c"
#include <string.h>

int main(int argc, char *argv[])
{   

    char *nomeDev;
    // para formatar executar com -f
    if(argc == 4 && (strcmp(argv[1], "-f")==0)){
        printf("formatando...\n");
        fs_formatar(argv[2], atoi(argv[3]));
        printf("bla.data formatado com sucesso!\n");
        return 0;
    }
    else{
        if(argc == 2){
            nomeDev = argv[1];
        }
        else{
            printf("Para rodar os testes execute ./test <<nome do dispositivo ja formatado>>\n");
            printf("Para formatar um dispositivo execute ./test -f <<nome do arquivo ja formatado>> <<tamanho de bloco desejado>>\n");
            return -1;
        }
    }

    
    printf("Abrindo dispositivo...\n");
    dev_abrir(nomeDev);
    printf("Extraindo configuração do dispositivo...\n");
    if(fs_carregar_config() == -1){
        return -1;
    }
    printf("SISTEMA RECUPERADO COM SUCESSO!\n");

    printf("RODANDO TESTES, ao final você deverá ter mais um arquivo no diretorio raiz\n");
    printf("Testanto insercao de arquivo no diretorio raiz...\n");

    int arquivo = obtem_bloco_vazio();
    criaArquivoText("arquivo.txt", arquivo, dev.raiz);
    printf("\n");
    printDir(dev.raiz);

    printf("Ok\n\n");


    printf("Testanto insercao de diretorio no diretorio raiz...\n");

    int dir = obtem_bloco_vazio();
    criaDir("dir", dir, dev.raiz);
    printf("\n");
    printDir(dev.raiz);

    printf("Ok\n\n");

    printf("Testanto insercao de arquivos no diretorio novo...\n");

    dev.dirAtual = dir;

    int arquivo1 = obtem_bloco_vazio();
    criaArquivoText("arquivo1.txt", arquivo1, dev.dirAtual);
    int arquivo2 = obtem_bloco_vazio();
    criaArquivoText("arquivo2.txt", arquivo2, dev.dirAtual);
    printf("\n");
    printDir(dev.dirAtual);

    printf("Ok\n\n");


    printf("Testanto exclusao de arquivo no diretorio novo...\n");

    delArquivo(arquivo1);

    printf("\n");
    printDir(dev.dirAtual);

    printf("Ok\n\n");

    printf("Testanto exclusao de diretorio no diretorio raiz...\n");

    delDir(dev.dirAtual);

    dev.dirAtual = dev.raiz;

    printf("\n");
    printDir(dev.raiz);

    printf("Ok\n\n");
    printf("FIM\n");
}
